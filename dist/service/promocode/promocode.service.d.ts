import { BasicService } from '../../helper/basic.service';
import { TimeSharePromocode } from '../../model/promocode/promocode.interface';
import { Db, ObjectId } from 'mongodb';
import { TimeShareClientService } from '../client/client.service';
export declare class TimeSharePromocodeService extends BasicService<TimeSharePromocode> {
    private database;
    private clientService;
    constructor(database: Db, clientService: TimeShareClientService);
    use(args: {
        code: string;
        clientId: ObjectId;
    }): Promise<number | false>;
}
