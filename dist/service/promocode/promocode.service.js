"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimeSharePromocodeService = void 0;
const common_1 = require("@nestjs/common");
const basic_service_1 = require("../../helper/basic.service");
const database_provider_1 = require("../../helper/database/database.provider");
const mongodb_1 = require("mongodb");
const client_service_1 = require("../client/client.service");
const apollo_server_core_1 = require("apollo-server-core");
let TimeSharePromocodeService = class TimeSharePromocodeService extends basic_service_1.BasicService {
    constructor(database, clientService) {
        super();
        this.database = database;
        this.clientService = clientService;
        this.dbService = this.database.collection('promocode');
        this.dbService.createIndex({ code: 1 }, { unique: true });
    }
    use(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const { code, clientId } = args;
            const promocode = yield this.dbService.findOne({
                code,
                clientId: {
                    $not: {
                        $elemMatch: { $eq: clientId },
                    },
                },
            });
            if (!promocode)
                throw new apollo_server_core_1.ApolloError('promocode not found');
            const currentDate = new Date();
            if (promocode.allowedNumOfClients) {
                if (promocode.allowedNumOfClients == 0)
                    throw new apollo_server_core_1.ApolloError('limit of people');
                this.updateOne({
                    find: { _id: promocode._id },
                    update: { allowedNumOfClients: -1 },
                    method: '$inc',
                });
            }
            if (promocode.endDate) {
                if (promocode.endDate < currentDate)
                    throw new apollo_server_core_1.ApolloError('passed due');
            }
            const client = yield this.clientService.updateOne({
                find: { _id: clientId },
                update: { bonus: promocode.bonus },
                method: '$inc',
            });
            if (!client)
                return false;
            this.updateOneWithOptions({
                findField: ['_id'],
                findValue: [promocode._id],
                updateField: ['clientId'],
                updateValue: [client._id],
                method: '$addToSet',
            });
            return promocode.bonus;
        });
    }
};
TimeSharePromocodeService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(database_provider_1.DATABASE_CONNECTION)),
    __metadata("design:paramtypes", [mongodb_1.Db,
        client_service_1.TimeShareClientService])
], TimeSharePromocodeService);
exports.TimeSharePromocodeService = TimeSharePromocodeService;
