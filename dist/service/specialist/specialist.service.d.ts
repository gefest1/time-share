import { Specialist } from '../../model/specialist/specialist.interface';
import { BasicService } from '../../helper/basic.service';
import { Db } from 'mongodb';
export declare class TimeShareSpecialistService extends BasicService<Specialist> {
    private database;
    constructor(database: Db);
}
