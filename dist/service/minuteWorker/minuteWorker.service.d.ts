import { MinuteWorker } from '../../model/minuteWorker/minuteWorker.interface';
import { BasicService } from '../../helper/basic.service';
import { Db } from 'mongodb';
export declare class MinuteWorkerService extends BasicService<MinuteWorker> {
    private database;
    constructor(database: Db);
}
