import { Db } from 'mongodb';
import { BasicService } from '../../helper/basic.service';
import { MinuteOwner } from '../../model/minuteOwner/minuteOwner.interface';
export declare class MinuteOwnerService extends BasicService<MinuteOwner> {
    private database;
    constructor(database: Db);
}
