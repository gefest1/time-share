import { Client } from '../../model/client/client.interface';
import { Db } from 'mongodb';
import { BasicService } from '../../helper/basic.service';
export declare class TimeShareClientService extends BasicService<Client> {
    private database;
    constructor(database: Db);
}
