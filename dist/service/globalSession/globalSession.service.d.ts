import { TimeShareGlobalSession } from '../../model/globalSession/globalSession.interface';
import { BasicService } from '../../helper/basic.service';
import { Db } from 'mongodb';
export declare class TimeShareGlobalSessionService extends BasicService<TimeShareGlobalSession> {
    private database;
    constructor(database: Db);
}
