export declare type roleTypes = 'specialist' | 'client' | 'owner' | 'client' | 'worker';
export declare const Roles: (...roles: roleTypes[]) => import("@nestjs/common").CustomDecorator<string>;
export declare const matchRoles: (userRole: roleTypes, roles: string[]) => boolean;
