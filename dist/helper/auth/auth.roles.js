"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.matchRoles = exports.Roles = void 0;
const common_1 = require("@nestjs/common");
const Roles = (...roles) => (0, common_1.SetMetadata)('roles', roles);
exports.Roles = Roles;
const matchRoles = function (userRole, roles) {
    const checkRole = roles.find((val) => {
        return val === userRole;
    });
    if (!checkRole)
        return false;
    return true;
};
exports.matchRoles = matchRoles;
