"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrentTokenPayloadSubscription = exports.CurrentTokenPayload = exports.CurrentUserSubscription = exports.CurrentMinuteWorkerSubscription = exports.CurrentMinuteOwnerSubscription = exports.CurrentSpecialistSubscription = exports.CurrentClientSubscription = exports.CurrentSpecialist = exports.CurrentMinuteWorker = exports.CurrentMinuteOwner = exports.CurrentClient = exports.CurrentUser = exports.PreAuthGuardSubscription = exports.PreAuthGuard = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const graphql_1 = require("@nestjs/graphql");
const mongodb_1 = require("mongodb");
const client_service_1 = require("../../service/client/client.service");
const minuteOwner_service_1 = require("../../service/minuteOwner/minuteOwner.service");
const minuteWorker_1 = require("../../service/minuteWorker");
const specialist_service_1 = require("../../service/specialist/specialist.service");
const token_service_1 = require("../token/token.service");
const auth_roles_1 = require("./auth.roles");
let PreAuthGuard = class PreAuthGuard {
    constructor(clientService, minuteOwnerService, specialistService, minuteWorkerService, reflector) {
        this.clientService = clientService;
        this.minuteOwnerService = minuteOwnerService;
        this.specialistService = specialistService;
        this.minuteWorkerService = minuteWorkerService;
        this.reflector = reflector;
    }
    canActivate(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const request = graphql_1.GqlExecutionContext.create(context).getContext().req.headers;
            const { authorization } = request;
            if (!authorization)
                return false;
            try {
                const payload = (0, token_service_1.verifyToken)(authorization);
                if (!payload)
                    return false;
                request.payload = payload;
                const roles = this.reflector.get('roles', context.getHandler());
                const checkRoles = (0, auth_roles_1.matchRoles)(payload.role, roles);
                if (!checkRoles)
                    return false;
                if (payload.role === 'client') {
                    const client = payload._id
                        ? yield this.clientService.findOne({ _id: new mongodb_1.ObjectId(payload._id) })
                        : yield this.clientService.findOne({
                            phoneNumber: payload.phoneNumber,
                        });
                    if (!client)
                        return false;
                    request.user = client;
                    request.client = client;
                }
                if (payload.role === 'owner') {
                    const minuteOwner = yield this.minuteOwnerService.findOne({
                        _id: new mongodb_1.ObjectId(payload._id),
                    });
                    if (!minuteOwner)
                        return false;
                    request.user = minuteOwner;
                    request.minuteOwner = minuteOwner;
                }
                if (payload.role === 'specialist') {
                    const specialist = yield this.specialistService.findOne({
                        _id: new mongodb_1.ObjectId(payload._id),
                    });
                    if (!specialist)
                        return false;
                    request.user = specialist;
                    request.specialist = specialist;
                }
                if (payload.role === 'worker') {
                    const minuteWorker = yield this.minuteWorkerService.findOne({
                        _id: new mongodb_1.ObjectId(payload._id),
                    });
                    request.user = minuteWorker;
                    request.minuteWorker = minuteWorker;
                }
                return true;
            }
            catch (e) {
                throw `${e}`;
            }
        });
    }
};
PreAuthGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [client_service_1.TimeShareClientService,
        minuteOwner_service_1.MinuteOwnerService,
        specialist_service_1.TimeShareSpecialistService,
        minuteWorker_1.MinuteWorkerService,
        core_1.Reflector])
], PreAuthGuard);
exports.PreAuthGuard = PreAuthGuard;
let PreAuthGuardSubscription = class PreAuthGuardSubscription {
    constructor(reflector, clientService, minuteOwnerService, specialistService, minuteWorkerService) {
        this.reflector = reflector;
        this.clientService = clientService;
        this.minuteOwnerService = minuteOwnerService;
        this.specialistService = specialistService;
        this.minuteWorkerService = minuteWorkerService;
    }
    canActivate(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const request = graphql_1.GqlExecutionContext.create(context).getContext();
            const { Authorization } = request;
            if (!Authorization)
                return false;
            try {
                const payload = (0, token_service_1.verifyToken)(Authorization);
                if (!payload)
                    return false;
                request.payload = payload;
                const roles = this.reflector.get('roles', context.getHandler());
                const checkRoles = (0, auth_roles_1.matchRoles)(payload.role, roles);
                if (!checkRoles)
                    return false;
                if (payload.role === 'client') {
                    const client = payload._id
                        ? yield this.clientService.findOne({ _id: new mongodb_1.ObjectId(payload._id) })
                        : yield this.clientService.findOne({
                            phoneNumber: payload.phoneNumber,
                        });
                    if (!client)
                        return false;
                    request.user = client;
                    request.client = client;
                }
                if (payload.role === 'owner') {
                    const minuteOwner = yield this.minuteOwnerService.findOne({
                        _id: new mongodb_1.ObjectId(payload._id),
                    });
                    if (!minuteOwner)
                        return false;
                    request.user = minuteOwner;
                    request.minuteOwner = minuteOwner;
                }
                if (payload.role === 'specialist') {
                    const specialist = yield this.specialistService.findOne({
                        _id: new mongodb_1.ObjectId(payload._id),
                    });
                    if (!specialist)
                        return false;
                    request.user = specialist;
                    request.specialist = specialist;
                }
                if (payload.role === 'worker') {
                    const minuteWorker = yield this.minuteWorkerService.findOne({
                        _id: new mongodb_1.ObjectId(payload._id),
                    });
                    request.user = minuteWorker;
                    request.minuteWorker = minuteWorker;
                }
                return true;
            }
            catch (e) {
                throw `${e}`;
            }
        });
    }
};
PreAuthGuardSubscription = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [core_1.Reflector,
        client_service_1.TimeShareClientService,
        minuteOwner_service_1.MinuteOwnerService,
        specialist_service_1.TimeShareSpecialistService,
        minuteWorker_1.MinuteWorkerService])
], PreAuthGuardSubscription);
exports.PreAuthGuardSubscription = PreAuthGuardSubscription;
exports.CurrentUser = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext().req.headers;
    return request.user;
}));
exports.CurrentClient = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext().req.headers;
    return request.client;
}));
exports.CurrentMinuteOwner = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext().req.headers;
    return request.minuteOwner;
}));
exports.CurrentMinuteWorker = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext().req.headers;
    return request.minuteWorker;
}));
exports.CurrentSpecialist = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext().req.headers;
    return request.specialist;
}));
exports.CurrentClientSubscription = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext();
    return request.client;
}));
exports.CurrentSpecialistSubscription = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext();
    return request.specialist;
}));
exports.CurrentMinuteOwnerSubscription = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext();
    return request.minuteOwner;
}));
exports.CurrentMinuteWorkerSubscription = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext();
    return request.minuteWorker;
}));
exports.CurrentUserSubscription = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext();
    return request.user;
}));
exports.CurrentTokenPayload = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext().req.headers;
    return request.payload;
}));
exports.CurrentTokenPayloadSubscription = (0, common_1.createParamDecorator)((data, context) => __awaiter(void 0, void 0, void 0, function* () {
    const request = graphql_1.GqlExecutionContext.create(context).getContext();
    return request.payload;
}));
