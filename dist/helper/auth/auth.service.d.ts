import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { TimeShareClientService } from '../../service/client/client.service';
import { MinuteOwnerService } from '../../service/minuteOwner/minuteOwner.service';
import { MinuteWorkerService } from '../../service/minuteWorker';
import { TimeShareSpecialistService } from '../../service/specialist/specialist.service';
export declare class PreAuthGuard implements CanActivate {
    private clientService;
    private minuteOwnerService;
    private specialistService;
    private minuteWorkerService;
    private reflector;
    constructor(clientService: TimeShareClientService, minuteOwnerService: MinuteOwnerService, specialistService: TimeShareSpecialistService, minuteWorkerService: MinuteWorkerService, reflector: Reflector);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
export declare class PreAuthGuardSubscription implements CanActivate {
    private reflector;
    private clientService;
    private minuteOwnerService;
    private specialistService;
    private minuteWorkerService;
    constructor(reflector: Reflector, clientService: TimeShareClientService, minuteOwnerService: MinuteOwnerService, specialistService: TimeShareSpecialistService, minuteWorkerService: MinuteWorkerService);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
export declare const CurrentUser: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const CurrentClient: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const CurrentMinuteOwner: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const CurrentMinuteWorker: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const CurrentSpecialist: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const CurrentClientSubscription: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const CurrentSpecialistSubscription: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const CurrentMinuteOwnerSubscription: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const CurrentMinuteWorkerSubscription: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const CurrentUserSubscription: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const CurrentTokenPayload: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare const CurrentTokenPayloadSubscription: (...dataOrPipes: unknown[]) => ParameterDecorator;
