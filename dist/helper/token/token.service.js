"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createToken = exports.verifyToken = void 0;
const jwt = require("jsonwebtoken");
const posix_1 = require("path/posix");
const fs = require("fs");
const token_interface_1 = require("./token.interface");
const verifyToken = function (token) {
    const publicKeyPath = (0, posix_1.join)(process.cwd(), 'publicKey.cer');
    const publicKey = fs.readFileSync(publicKeyPath, {
        encoding: 'utf-8',
    });
    const tokenObj = jwt.verify(token, publicKey);
    return tokenObj;
};
exports.verifyToken = verifyToken;
const createToken = function (payload) {
    const privateKeyPath = (0, posix_1.join)(process.cwd(), 'privateKey.cer');
    const privateKey = fs.readFileSync(privateKeyPath, {
        encoding: 'utf-8',
    });
    const token = jwt.sign(payload, privateKey, token_interface_1.signOption);
    return token;
};
exports.createToken = createToken;
