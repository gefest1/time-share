"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.signOption = void 0;
exports.signOption = {
    issuer: '',
    subject: '',
    audience: '',
    expiresIn: '365d',
    algorithm: 'RS256',
};
