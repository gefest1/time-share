import { roleTypes } from '../auth/auth.roles';
import * as jwt from 'jsonwebtoken';
export interface Token {
    _id?: string;
    phoneNumber: string;
    role: roleTypes;
}
export declare const signOption: jwt.SignOptions;
