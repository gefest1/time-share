import { Token } from './token.interface';
export declare const verifyToken: (token: string) => Token;
export declare const createToken: (payload: Token) => string;
