"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseProvider = exports.DATABASE_CONNECTION = void 0;
const config_1 = require("@nestjs/config");
const mongodb_1 = require("mongodb");
exports.DATABASE_CONNECTION = 'DATABASE_CONNECTION';
exports.DatabaseProvider = {
    provide: 'DATABASE_CONNECTION',
    useFactory: (configService) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const client = yield mongodb_1.MongoClient.connect(configService.get('MONGO_URL'));
            const database = client.db(configService.get('MONGO_DB_NAME'));
            return database;
        }
        catch (e) {
            throw e;
        }
    }),
    inject: [config_1.ConfigService],
};
