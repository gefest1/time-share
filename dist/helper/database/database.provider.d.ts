import { FactoryProvider } from '@nestjs/common';
import { Db } from 'mongodb';
export declare const DATABASE_CONNECTION = "DATABASE_CONNECTION";
export declare const DatabaseProvider: FactoryProvider<Promise<Db>>;
