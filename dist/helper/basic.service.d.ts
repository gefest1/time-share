import { AggregationCursor, Collection, Document, Filter, FindCursor, ObjectId } from 'mongodb';
export declare abstract class BasicService<T extends Document> {
    protected dbService: Collection<T>;
    basicLookups: {
        from: string;
        localField?: string;
        foreignField?: string;
        let?: {
            [index: string]: keyof T;
        };
        pipeline?: any[];
        as: string;
        isArray: boolean;
    }[];
    findOne(args: Partial<T>): Promise<T>;
    find(args: Partial<T>): Promise<T[]>;
    findOneWithOptions(args: {
        fields: (keyof T)[];
        values: any[];
    }): Promise<T | null>;
    findWithOptions(args: {
        fields: (keyof T)[];
        values: any[];
    }): Promise<T[]>;
    updateOne(args: {
        find: Partial<T>;
        update: Partial<T>;
        method: '$set' | '$inc' | '$push' | '$addToSet' | '$pull' | '$unset';
        ignoreUndefined?: true;
    }): Promise<T>;
    updateOneWithOptions(args: {
        findField: (keyof T)[];
        findValue: any[];
        updateField: (keyof T)[];
        updateValue: any[];
        method: '$set' | '$inc' | '$push' | '$addToSet' | '$pull' | '$unset';
        ignoreUndefined?: true;
    }): Promise<import("mongodb").WithId<T>>;
    updateManyWithOptions(args: {
        findField: (keyof T)[];
        findValue: any[];
        updateField: (keyof T)[];
        updateValue: any[];
        method: '$set' | '$inc' | '$push' | '$addToSet' | '$pull' | '$unset';
    }): Promise<void>;
    insertOne(args: T): Promise<ObjectId>;
    findCursor(args: Partial<T>): FindCursor<T>;
    findWithAddictivesCursor<U>(args: {
        find?: Partial<T>;
        matchQuery?: Partial<{
            [Property in keyof T]: any;
        }>;
        sort?: Partial<{
            [Property in keyof T]: number;
        }>;
        lookups: {
            from: string;
            localField?: string;
            foreignField?: string;
            let?: {
                [index: string]: keyof T;
            };
            pipeline?: any[];
            as: string;
            isArray: boolean;
        }[];
    }): AggregationCursor<U>;
    getMaxPages(args: {
        elementsPerPage: number;
        maxNumOfElements: number;
        find?: Filter<T>;
    }): Promise<number>;
    list(): Promise<import("mongodb").WithId<T>[]>;
}
