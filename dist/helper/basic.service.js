"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BasicService = void 0;
class BasicService {
    findOne(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const document = yield this.dbService.findOne(args);
            return document;
        });
    }
    find(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const documents = yield this.dbService.find(args).toArray();
            return documents;
        });
    }
    findOneWithOptions(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const { fields, values } = args;
            const query = {};
            fields.map((val, ind) => (query[val] = values[ind]));
            const document = yield this.dbService.findOne(query);
            return document;
        });
    }
    findWithOptions(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const { fields, values } = args;
            const query = {};
            fields.map((val, ind) => (query[val] = values[ind]));
            const documents = yield this.dbService.find(query).toArray();
            return documents;
        });
    }
    updateOne(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const { find, update, method, ignoreUndefined } = args;
            const updateQuery = {
                [method]: update,
            };
            const document = yield this.dbService.findOneAndUpdate(find, updateQuery, {
                returnDocument: 'after',
                ignoreUndefined: ignoreUndefined ? ignoreUndefined : false,
            });
            return document.value;
        });
    }
    updateOneWithOptions(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const { findField, findValue, updateField, updateValue, method, ignoreUndefined, } = args;
            const findQuery = {};
            findField.map((val, ind) => (findQuery[val] = findValue[ind]));
            const updateFieldsValues = {};
            updateField.map((val, ind) => (updateFieldsValues[val] = updateValue[ind]));
            const updateQuery = {
                [method]: updateFieldsValues,
            };
            const document = yield this.dbService.findOneAndUpdate(findQuery, updateQuery, {
                returnDocument: 'after',
                ignoreUndefined: ignoreUndefined ? ignoreUndefined : false,
            });
            return document.value;
        });
    }
    updateManyWithOptions(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const { findField, findValue, updateField, updateValue, method } = args;
            const findQuery = {};
            findField.map((val, ind) => (findQuery[val] = findValue[ind]));
            const updateFieldsValues = {};
            updateField.map((val, ind) => (updateFieldsValues[val] = updateValue[ind]));
            const updateQuery = {
                [method]: updateFieldsValues,
            };
            yield this.dbService.updateMany(findQuery, updateQuery);
        });
    }
    insertOne(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const document = yield this.dbService.insertOne(args);
            return document.insertedId;
        });
    }
    findCursor(args) {
        const cursor = this.dbService.find(args);
        return cursor;
    }
    findWithAddictivesCursor(args) {
        const { find, sort, lookups: _lookups, matchQuery } = args;
        const notArrayLookups = _lookups.filter((val) => val.isArray === false);
        const lookups = _lookups.map((val) => {
            const lookupQuery = val.let
                ? {
                    $lookup: {
                        from: val.from,
                        let: {
                            [Object.keys(val.let)[0]]: `$${val.let[Object.keys(val.let)[0]]}`,
                        },
                        pipeline: val.pipeline,
                        as: val.as,
                    },
                }
                : {
                    $lookup: {
                        from: val.from,
                        localField: val.localField,
                        foreignField: val.foreignField,
                        as: val.as,
                    },
                };
            return lookupQuery;
        });
        const addFields = {};
        notArrayLookups.map((val) => {
            addFields[val.as] = {
                $first: `$${val.as}`,
            };
        });
        const aggregationUnfiltered = [
            { $match: find ? find : matchQuery },
            ...lookups,
            { $addFields: addFields },
            sort && { $sort: sort },
        ];
        const aggregation = aggregationUnfiltered.filter((val) => val !== undefined);
        const cursor = this.dbService.aggregate(aggregation);
        return cursor;
    }
    getMaxPages(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const { find, elementsPerPage, maxNumOfElements } = args;
            const documents = yield this.dbService.countDocuments(find, {
                limit: maxNumOfElements,
            });
            const pages = Math.round(documents / elementsPerPage);
            return pages;
        });
    }
    list() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.dbService.find().toArray();
        });
    }
}
exports.BasicService = BasicService;
