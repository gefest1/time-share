export interface PhotoUrl {
    thumbnail: string;
    XL: string;
    M: string;
}
export declare class PhotoUrlGraph implements PhotoUrl {
    XL: string;
    M: string;
    thumbnail: string;
    constructor(photoUrl: Partial<PhotoUrl>);
}
