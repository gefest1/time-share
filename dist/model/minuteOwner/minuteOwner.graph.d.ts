import { MinuteInstitution } from '../minuteInstitution/minuteInstitution.interface';
import { Modify } from '../../utils/modify.type';
import { MinuteInstitutionGraph } from '../minuteInstitution/minuteInstitution.graph';
import { PhotoUrlGraph } from '../photoUrl/photoUrl.graph';
import { MinuteOwner } from './minuteOwner.interface';
export declare class MinuteOwnerGraph implements Modify<Omit<MinuteOwner, 'passwordHASH'>, {
    _id: string;
}> {
    _id: string;
    fullName: string;
    email: string;
    phoneNumber: string;
    token: string;
    isNotApproved: boolean;
    typeOfBusiness: string;
    nameOfTheOrganization: string;
    photoURL: PhotoUrlGraph;
    dateOfRequest: Date;
    institutions: MinuteInstitutionGraph[];
    constructor(owner: Partial<MinuteOwner> & {
        token?: string;
        institutions?: MinuteInstitution[];
    });
}
