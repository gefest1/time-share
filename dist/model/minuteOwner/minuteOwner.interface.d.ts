import { ObjectId } from 'mongodb';
import { PhotoUrl } from '../photoUrl/photoUrl.interface';
export interface MinuteOwner {
    _id: ObjectId;
    fullName: string;
    email: string;
    phoneNumber: string;
    passwordHASH: string;
    isNotApproved?: boolean;
    typeOfBusiness?: string;
    nameOfTheOrganization?: string;
    photoURL?: PhotoUrl;
    dateOfRequest: Date;
}
