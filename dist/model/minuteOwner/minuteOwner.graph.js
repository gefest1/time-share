"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MinuteOwnerGraph = void 0;
const graphql_1 = require("@nestjs/graphql");
const minuteInstitution_graph_1 = require("../minuteInstitution/minuteInstitution.graph");
const photoUrl_graph_1 = require("../photoUrl/photoUrl.graph");
let MinuteOwnerGraph = class MinuteOwnerGraph {
    constructor(owner) {
        if (owner._id != null)
            this._id = owner._id.toHexString();
        if (owner.email != null)
            this.email = owner.email;
        if (owner.fullName != null)
            this.fullName = owner.fullName;
        if (owner.phoneNumber != null)
            this.phoneNumber = owner.phoneNumber;
        if (owner.token != null)
            this.token = owner.token;
        if (owner.isNotApproved != null)
            this.isNotApproved = owner.isNotApproved;
        if (owner.typeOfBusiness != null)
            this.typeOfBusiness = owner.typeOfBusiness;
        if (owner.nameOfTheOrganization != null)
            this.nameOfTheOrganization = owner.nameOfTheOrganization;
        if (owner.photoURL != null)
            this.photoURL = new photoUrl_graph_1.PhotoUrlGraph(Object.assign({}, owner.photoURL));
        if (owner.institutions && owner.institutions[0] != null)
            this.institutions = owner.institutions.map((val) => new minuteInstitution_graph_1.MinuteInstitutionGraph(Object.assign({}, val)));
    }
};
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteOwnerGraph.prototype, "_id", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteOwnerGraph.prototype, "fullName", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteOwnerGraph.prototype, "email", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteOwnerGraph.prototype, "phoneNumber", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], MinuteOwnerGraph.prototype, "token", void 0);
__decorate([
    (0, graphql_1.Field)(() => Boolean, { nullable: true }),
    __metadata("design:type", Boolean)
], MinuteOwnerGraph.prototype, "isNotApproved", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], MinuteOwnerGraph.prototype, "typeOfBusiness", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], MinuteOwnerGraph.prototype, "nameOfTheOrganization", void 0);
__decorate([
    (0, graphql_1.Field)(() => photoUrl_graph_1.PhotoUrlGraph, { nullable: true }),
    __metadata("design:type", photoUrl_graph_1.PhotoUrlGraph)
], MinuteOwnerGraph.prototype, "photoURL", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.GraphQLISODateTime),
    __metadata("design:type", Date)
], MinuteOwnerGraph.prototype, "dateOfRequest", void 0);
__decorate([
    (0, graphql_1.Field)(() => [minuteInstitution_graph_1.MinuteInstitutionGraph]),
    __metadata("design:type", Array)
], MinuteOwnerGraph.prototype, "institutions", void 0);
MinuteOwnerGraph = __decorate([
    (0, graphql_1.ObjectType)('Owner'),
    __metadata("design:paramtypes", [Object])
], MinuteOwnerGraph);
exports.MinuteOwnerGraph = MinuteOwnerGraph;
