import { ObjectId } from 'mongodb';
import { Sex } from '../client';
import { PhotoUrl } from '../photoUrl/photoUrl.interface';
import { SpecialistContacts } from './utils/contacts.model';
import { SpecialistDocument } from './utils/document.model';
import { SpecialistWorkingInfo } from './utils/workInfo.model';
export interface Specialist {
    _id: ObjectId;
    fullName: string;
    phoneNumber: string;
    sex: Sex;
    dateOfBirth: Date;
    description: string;
    photoURL?: PhotoUrl;
    videoUrl?: string;
    certifications?: PhotoUrl[];
    contacts?: SpecialistContacts[];
    email?: string;
    instituionIds?: ObjectId[];
    document?: SpecialistDocument;
    workInfo?: SpecialistWorkingInfo;
}
