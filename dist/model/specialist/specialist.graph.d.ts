import { Modify } from '../../utils/modify.type';
import { Sex } from '../client/client.enum';
import { ContactsGraph } from '../contacts/contacts.graph';
import { PhotoUrlGraph } from '../photoUrl/photoUrl.graph';
import { Specialist } from './specialist.interface';
import { SpecialistWorkingInfoGraph } from './utils/workInfo.model';
export declare class SpecialistGraph implements Modify<Specialist, {
    _id: string;
    instituionIds?: string[];
    workInfo?: SpecialistWorkingInfoGraph;
}> {
    _id: string;
    fullName: string;
    phoneNumber: string;
    sex: Sex;
    description: string;
    videoUrl: string;
    certifications: PhotoUrlGraph[];
    contacts: ContactsGraph[];
    email: string;
    dateOfBirth: Date;
    photoURL: PhotoUrlGraph;
    instituionIds?: string[];
    workInfo?: SpecialistWorkingInfoGraph;
    constructor(specialist: Partial<Specialist>);
}
