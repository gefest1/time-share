"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllowedWorkingTimeSpecialistInfo = exports.AllowedWorkingNameSpecialistInfo = void 0;
const graphql_1 = require("@nestjs/graphql");
var AllowedWorkingNameSpecialistInfo;
(function (AllowedWorkingNameSpecialistInfo) {
    AllowedWorkingNameSpecialistInfo["Company"] = "company";
    AllowedWorkingNameSpecialistInfo["Private"] = "private";
})(AllowedWorkingNameSpecialistInfo = exports.AllowedWorkingNameSpecialistInfo || (exports.AllowedWorkingNameSpecialistInfo = {}));
var AllowedWorkingTimeSpecialistInfo;
(function (AllowedWorkingTimeSpecialistInfo) {
    AllowedWorkingTimeSpecialistInfo["Always"] = "always";
    AllowedWorkingTimeSpecialistInfo["Sometimes"] = "sometimes";
    AllowedWorkingTimeSpecialistInfo["DontKnow"] = "dontKnow";
})(AllowedWorkingTimeSpecialistInfo = exports.AllowedWorkingTimeSpecialistInfo || (exports.AllowedWorkingTimeSpecialistInfo = {}));
(0, graphql_1.registerEnumType)(AllowedWorkingNameSpecialistInfo, {
    name: 'AllowedWorkingNameSpecialistInfo',
});
(0, graphql_1.registerEnumType)(AllowedWorkingTimeSpecialistInfo, {
    name: 'AllowedWorkingTimeSpecialistInfo',
});
