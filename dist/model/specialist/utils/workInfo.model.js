"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpecialistWorkingInfoInput = exports.SpecialistWorkingInfoGraph = void 0;
const graphql_1 = require("@nestjs/graphql");
const workInfo_enum_1 = require("./workInfo.enum");
let SpecialistWorkingInfoGraph = class SpecialistWorkingInfoGraph {
    constructor(args) {
        if (args.workingName != null)
            this.workingName = args.workingName;
        if (args.workingTime != null)
            this.workingTime = args.workingTime;
    }
};
__decorate([
    (0, graphql_1.Field)(() => workInfo_enum_1.AllowedWorkingNameSpecialistInfo),
    __metadata("design:type", String)
], SpecialistWorkingInfoGraph.prototype, "workingName", void 0);
__decorate([
    (0, graphql_1.Field)(() => workInfo_enum_1.AllowedWorkingTimeSpecialistInfo),
    __metadata("design:type", String)
], SpecialistWorkingInfoGraph.prototype, "workingTime", void 0);
SpecialistWorkingInfoGraph = __decorate([
    (0, graphql_1.ObjectType)(),
    __metadata("design:paramtypes", [Object])
], SpecialistWorkingInfoGraph);
exports.SpecialistWorkingInfoGraph = SpecialistWorkingInfoGraph;
let SpecialistWorkingInfoInput = class SpecialistWorkingInfoInput {
};
__decorate([
    (0, graphql_1.Field)(() => workInfo_enum_1.AllowedWorkingNameSpecialistInfo),
    __metadata("design:type", String)
], SpecialistWorkingInfoInput.prototype, "workingName", void 0);
__decorate([
    (0, graphql_1.Field)(() => workInfo_enum_1.AllowedWorkingTimeSpecialistInfo),
    __metadata("design:type", String)
], SpecialistWorkingInfoInput.prototype, "workingTime", void 0);
SpecialistWorkingInfoInput = __decorate([
    (0, graphql_1.InputType)()
], SpecialistWorkingInfoInput);
exports.SpecialistWorkingInfoInput = SpecialistWorkingInfoInput;
