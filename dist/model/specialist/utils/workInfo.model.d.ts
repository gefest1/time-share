import { AllowedWorkingNameSpecialistInfo, AllowedWorkingTimeSpecialistInfo } from './workInfo.enum';
export interface SpecialistWorkingInfo {
    workingName: AllowedWorkingNameSpecialistInfo;
    workingTime: AllowedWorkingTimeSpecialistInfo;
}
export declare class SpecialistWorkingInfoGraph implements SpecialistWorkingInfo {
    workingName: AllowedWorkingNameSpecialistInfo;
    workingTime: AllowedWorkingTimeSpecialistInfo;
    constructor(args: Partial<SpecialistWorkingInfo>);
}
export declare class SpecialistWorkingInfoInput implements SpecialistWorkingInfo {
    workingName: AllowedWorkingNameSpecialistInfo;
    workingTime: AllowedWorkingTimeSpecialistInfo;
}
