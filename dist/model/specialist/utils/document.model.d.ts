import { ObjectId } from 'mongodb';
import { PhotoUrl } from '../../../model/photoUrl/photoUrl.interface';
export declare enum AllowedDocumentStatuses {
    Rejected = "rejected",
    Verified = "verified"
}
export interface SpecialistDocument {
    _id: ObjectId;
    countryCode: string;
    documentNumber: string;
    photoURLs: PhotoUrl[];
    status?: AllowedDocumentStatuses;
    dateOfRequest: Date;
    reasonOfRejection?: string;
}
