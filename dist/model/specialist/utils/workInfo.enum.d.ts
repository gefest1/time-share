export declare enum AllowedWorkingNameSpecialistInfo {
    Company = "company",
    Private = "private"
}
export declare enum AllowedWorkingTimeSpecialistInfo {
    Always = "always",
    Sometimes = "sometimes",
    DontKnow = "dontKnow"
}
