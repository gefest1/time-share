"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllowedDocumentStatuses = void 0;
const graphql_1 = require("@nestjs/graphql");
var AllowedDocumentStatuses;
(function (AllowedDocumentStatuses) {
    AllowedDocumentStatuses["Rejected"] = "rejected";
    AllowedDocumentStatuses["Verified"] = "verified";
})(AllowedDocumentStatuses = exports.AllowedDocumentStatuses || (exports.AllowedDocumentStatuses = {}));
(0, graphql_1.registerEnumType)(AllowedDocumentStatuses, {
    name: 'AllowedDocumentStatuses',
});
