"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpecialistGraph = void 0;
const graphql_1 = require("@nestjs/graphql");
const client_enum_1 = require("../client/client.enum");
const contacts_graph_1 = require("../contacts/contacts.graph");
const photoUrl_graph_1 = require("../photoUrl/photoUrl.graph");
const workInfo_model_1 = require("./utils/workInfo.model");
let SpecialistGraph = class SpecialistGraph {
    constructor(specialist) {
        if (specialist._id != null)
            this._id = specialist._id.toHexString();
        if (specialist.fullName != null)
            this.fullName = specialist.fullName;
        if (specialist.phoneNumber != null)
            this.phoneNumber = specialist.phoneNumber;
        if (specialist.sex != null)
            this.sex = specialist.sex;
        if (specialist.description != null)
            this.description = specialist.description;
        if (specialist.email != null)
            this.email = specialist.email;
        if (specialist.videoUrl != null)
            this.videoUrl = specialist.videoUrl;
        if (specialist.photoURL != null)
            this.photoURL = new photoUrl_graph_1.PhotoUrlGraph(Object.assign({}, specialist.photoURL));
        if (specialist.contacts != null)
            this.contacts = specialist.contacts.map((val) => new contacts_graph_1.ContactsGraph(Object.assign({}, val)));
        if (specialist.instituionIds != null)
            this.instituionIds = specialist.instituionIds.map((val) => val.toHexString());
        if (specialist.certifications != null)
            this.certifications = specialist.certifications.map((val) => new photoUrl_graph_1.PhotoUrlGraph(Object.assign({}, val)));
        if (specialist.dateOfBirth != null)
            this.dateOfBirth = specialist.dateOfBirth;
        if (specialist.workInfo != null)
            this.workInfo = new workInfo_model_1.SpecialistWorkingInfoGraph(Object.assign({}, specialist.workInfo));
    }
};
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], SpecialistGraph.prototype, "_id", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], SpecialistGraph.prototype, "fullName", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], SpecialistGraph.prototype, "phoneNumber", void 0);
__decorate([
    (0, graphql_1.Field)(() => client_enum_1.Sex),
    __metadata("design:type", String)
], SpecialistGraph.prototype, "sex", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], SpecialistGraph.prototype, "description", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], SpecialistGraph.prototype, "videoUrl", void 0);
__decorate([
    (0, graphql_1.Field)(() => photoUrl_graph_1.PhotoUrlGraph, { nullable: true }),
    __metadata("design:type", Array)
], SpecialistGraph.prototype, "certifications", void 0);
__decorate([
    (0, graphql_1.Field)(() => [contacts_graph_1.ContactsGraph], { nullable: true }),
    __metadata("design:type", Array)
], SpecialistGraph.prototype, "contacts", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], SpecialistGraph.prototype, "email", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.GraphQLISODateTime, { nullable: true }),
    __metadata("design:type", Date)
], SpecialistGraph.prototype, "dateOfBirth", void 0);
__decorate([
    (0, graphql_1.Field)(() => photoUrl_graph_1.PhotoUrlGraph, { nullable: true }),
    __metadata("design:type", photoUrl_graph_1.PhotoUrlGraph)
], SpecialistGraph.prototype, "photoURL", void 0);
__decorate([
    (0, graphql_1.Field)(() => [String], { nullable: true }),
    __metadata("design:type", Array)
], SpecialistGraph.prototype, "instituionIds", void 0);
__decorate([
    (0, graphql_1.Field)(() => workInfo_model_1.SpecialistWorkingInfoGraph, { nullable: true }),
    __metadata("design:type", workInfo_model_1.SpecialistWorkingInfoGraph)
], SpecialistGraph.prototype, "workInfo", void 0);
SpecialistGraph = __decorate([
    (0, graphql_1.ObjectType)('Specialist'),
    __metadata("design:paramtypes", [Object])
], SpecialistGraph);
exports.SpecialistGraph = SpecialistGraph;
