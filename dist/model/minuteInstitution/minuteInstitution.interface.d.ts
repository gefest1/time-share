import { ObjectId } from 'mongodb';
import { PhotoUrl } from '../photoUrl/photoUrl.interface';
import { Location } from '../location/location.interface';
import { AllowedMinuteInstitutionTypes } from './minuteInstitution.enum';
import { Tags } from './utils/tags.model';
import { Contacts } from '../contacts/contacts.interface';
import { WorkTime } from './utils/workTime.model';
export interface MinuteInstitution {
    _id: ObjectId;
    name: string;
    galleryURLs?: PhotoUrl[];
    avatarURL?: PhotoUrl;
    description?: string;
    type: AllowedMinuteInstitutionTypes;
    city: string;
    ratingSum?: number;
    ratingCount?: number;
    tags?: Tags[];
    dateAdded: Date;
    averagePrice?: number;
    contacts: Contacts;
    ownerId: ObjectId;
    workerIds?: ObjectId[];
    location: Location;
    videoURL?: string;
    workTimes?: WorkTime[];
    iconPath: string;
}
