"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkTimeGraph = void 0;
const graphql_1 = require("@nestjs/graphql");
let WorkTimeGraph = class WorkTimeGraph {
    constructor(workTime) {
        if (workTime._id != null)
            this._id = workTime._id.toHexString();
        if (workTime.endTime)
            this.endTime = workTime.endTime;
        if (workTime.startTime)
            this.startTime = workTime.startTime;
    }
};
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], WorkTimeGraph.prototype, "_id", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.GraphQLISODateTime),
    __metadata("design:type", Date)
], WorkTimeGraph.prototype, "startTime", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.GraphQLISODateTime),
    __metadata("design:type", Date)
], WorkTimeGraph.prototype, "endTime", void 0);
WorkTimeGraph = __decorate([
    (0, graphql_1.ObjectType)('WorkTime'),
    __metadata("design:paramtypes", [Object])
], WorkTimeGraph);
exports.WorkTimeGraph = WorkTimeGraph;
