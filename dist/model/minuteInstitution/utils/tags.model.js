"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TagsGraph = void 0;
const graphql_1 = require("@nestjs/graphql");
const minuteInstitution_enum_1 = require("../minuteInstitution.enum");
let TagsGraph = class TagsGraph {
    constructor(tags) {
        if (tags._id != null)
            this._id = tags._id.toHexString();
        if (tags.iconSVGPath != null)
            this.iconSVGPath = tags.iconSVGPath;
        if (tags.name != null)
            this.name = tags.name;
        if (tags.type != null)
            this.type = tags.type;
    }
};
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], TagsGraph.prototype, "_id", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], TagsGraph.prototype, "iconSVGPath", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], TagsGraph.prototype, "name", void 0);
__decorate([
    (0, graphql_1.Field)(() => [minuteInstitution_enum_1.AllowedMinuteInstitutionTypes]),
    __metadata("design:type", Array)
], TagsGraph.prototype, "type", void 0);
TagsGraph = __decorate([
    (0, graphql_1.ObjectType)('Tags'),
    __metadata("design:paramtypes", [Object])
], TagsGraph);
exports.TagsGraph = TagsGraph;
