import { ObjectId } from 'mongodb';
import { Modify } from '../../../utils/modify.type';
export interface WorkTime {
    _id: ObjectId;
    startTime: Date;
    endTime: Date;
}
export declare class WorkTimeGraph implements Modify<WorkTime, {
    _id: string;
}> {
    _id: string;
    startTime: Date;
    endTime: Date;
    constructor(workTime: Partial<WorkTime>);
}
