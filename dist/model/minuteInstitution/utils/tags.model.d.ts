import { ObjectId } from 'mongodb';
import { Modify } from '../../../utils/modify.type';
import { AllowedMinuteInstitutionTypes } from '../minuteInstitution.enum';
export interface Tags {
    _id: ObjectId;
    name: string;
    iconSVGPath: string;
    type: AllowedMinuteInstitutionTypes[];
}
export declare class TagsGraph implements Modify<Tags, {
    _id: string;
}> {
    _id: string;
    iconSVGPath: string;
    name: string;
    type: AllowedMinuteInstitutionTypes[];
    constructor(tags: Partial<Tags>);
}
