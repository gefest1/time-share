import { Modify } from '../../utils/modify.type';
import { ContactsGraph } from '../contacts/contacts.graph';
import { LocationGraph } from '../location/location.graph';
import { MinuteWorkerGraph } from '../minuteWorker/minuteWorker.graph';
import { MinuteWorker } from '../minuteWorker/minuteWorker.interface';
import { PhotoUrlGraph } from '../photoUrl/photoUrl.graph';
import { AllowedMinuteInstitutionTypes } from './minuteInstitution.enum';
import { MinuteInstitution } from './minuteInstitution.interface';
import { TagsGraph } from './utils/tags.model';
import { WorkTimeGraph } from './utils/workTime.model';
export declare class MinuteInstitutionGraph implements Modify<Omit<MinuteInstitution, 'ownerId' | 'ratingSum' | 'workerIds'>, {
    _id: string;
    dateAdded: string;
    galleryURLs: PhotoUrlGraph[];
    avatarURL: PhotoUrlGraph;
    contacts: ContactsGraph;
    location: LocationGraph;
    tags?: TagsGraph[];
    workTimes?: WorkTimeGraph[];
}> {
    _id: string;
    name: string;
    location: LocationGraph;
    averagePrice: number;
    galleryURLs: PhotoUrlGraph[];
    avatarURL: PhotoUrlGraph;
    description: string;
    city: string;
    ratingCount: number;
    contacts: ContactsGraph;
    rating: number;
    dateAdded: string;
    videoURL: string;
    type: AllowedMinuteInstitutionTypes;
    workers?: MinuteWorkerGraph[];
    numOfWorkers: number;
    workTimes?: WorkTimeGraph[];
    tags?: TagsGraph[];
    iconPath: string;
    constructor(institution: Partial<MinuteInstitution> & {
        workers?: MinuteWorker[];
        numOfWorkers?: number;
    });
}
