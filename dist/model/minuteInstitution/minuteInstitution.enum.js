"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllowedMinuteInstitutionTypes = void 0;
const graphql_1 = require("@nestjs/graphql");
var AllowedMinuteInstitutionTypes;
(function (AllowedMinuteInstitutionTypes) {
    AllowedMinuteInstitutionTypes["ActiveLeisure"] = "activeLeisure";
    AllowedMinuteInstitutionTypes["Fitness"] = "fitness";
    AllowedMinuteInstitutionTypes["Entertainment"] = "entertainment";
    AllowedMinuteInstitutionTypes["BusinessCenter"] = "businessCenter";
})(AllowedMinuteInstitutionTypes = exports.AllowedMinuteInstitutionTypes || (exports.AllowedMinuteInstitutionTypes = {}));
(0, graphql_1.registerEnumType)(AllowedMinuteInstitutionTypes, {
    name: 'AllowedInstitutionTypes',
});
