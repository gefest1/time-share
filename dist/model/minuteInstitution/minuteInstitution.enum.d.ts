export declare enum AllowedMinuteInstitutionTypes {
    ActiveLeisure = "activeLeisure",
    Fitness = "fitness",
    Entertainment = "entertainment",
    BusinessCenter = "businessCenter"
}
