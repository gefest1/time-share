"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MinuteInstitutionGraph = void 0;
const graphql_1 = require("@nestjs/graphql");
const contacts_graph_1 = require("../contacts/contacts.graph");
const location_graph_1 = require("../location/location.graph");
const minuteWorker_graph_1 = require("../minuteWorker/minuteWorker.graph");
const photoUrl_graph_1 = require("../photoUrl/photoUrl.graph");
const minuteInstitution_enum_1 = require("./minuteInstitution.enum");
const tags_model_1 = require("./utils/tags.model");
const workTime_model_1 = require("./utils/workTime.model");
let MinuteInstitutionGraph = class MinuteInstitutionGraph {
    constructor(institution) {
        if (institution._id != null)
            this._id = institution._id.toHexString();
        if (institution.avatarURL != null)
            this.avatarURL = new photoUrl_graph_1.PhotoUrlGraph(Object.assign({}, institution.avatarURL));
        if (institution.averagePrice != null)
            this.averagePrice = institution.averagePrice;
        if (institution.city != null)
            this.city = institution.city;
        if (institution.dateAdded != null)
            this.dateAdded = institution.dateAdded.toISOString();
        if (institution.galleryURLs != null)
            this.galleryURLs = institution.galleryURLs.map((val) => new photoUrl_graph_1.PhotoUrlGraph(Object.assign({}, val)));
        if (institution.description != null)
            this.description = institution.description;
        if (institution.ratingCount != null)
            this.ratingCount = institution.ratingCount;
        if (institution.ratingSum != null) {
            const rating = institution.ratingSum / institution.ratingCount;
            this.rating = Math.round(rating * 10) / 10;
        }
        if (institution.name != null)
            this.name = institution.name;
        if (institution.workers != null)
            this.workers = institution.workers.map((val) => new minuteWorker_graph_1.MinuteWorkerGraph(Object.assign({}, val)));
        if (institution.numOfWorkers != null)
            this.numOfWorkers = institution.numOfWorkers;
        if (institution.location != null)
            this.location = new location_graph_1.LocationGraph(Object.assign({}, institution.location));
        if (institution.contacts != null)
            this.contacts = new contacts_graph_1.ContactsGraph(Object.assign({}, institution.contacts));
        if (institution.videoURL != null)
            this.videoURL = institution.videoURL;
        if (institution.workTimes != null)
            this.workTimes = institution.workTimes.map((val) => new workTime_model_1.WorkTimeGraph(Object.assign({}, val)));
        if (institution.type != null)
            this.type = institution.type;
        if (institution.tags != null)
            this.tags = institution.tags.map((val) => new tags_model_1.TagsGraph(Object.assign({}, val)));
        if (institution.iconPath != null)
            this.iconPath = institution.iconPath;
    }
};
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteInstitutionGraph.prototype, "_id", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteInstitutionGraph.prototype, "name", void 0);
__decorate([
    (0, graphql_1.Field)(() => location_graph_1.LocationGraph),
    __metadata("design:type", location_graph_1.LocationGraph)
], MinuteInstitutionGraph.prototype, "location", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Int),
    __metadata("design:type", Number)
], MinuteInstitutionGraph.prototype, "averagePrice", void 0);
__decorate([
    (0, graphql_1.Field)(() => [photoUrl_graph_1.PhotoUrlGraph], { nullable: 'itemsAndList' }),
    __metadata("design:type", Array)
], MinuteInstitutionGraph.prototype, "galleryURLs", void 0);
__decorate([
    (0, graphql_1.Field)(() => photoUrl_graph_1.PhotoUrlGraph),
    __metadata("design:type", photoUrl_graph_1.PhotoUrlGraph)
], MinuteInstitutionGraph.prototype, "avatarURL", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], MinuteInstitutionGraph.prototype, "description", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteInstitutionGraph.prototype, "city", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Int, { defaultValue: 0 }),
    __metadata("design:type", Number)
], MinuteInstitutionGraph.prototype, "ratingCount", void 0);
__decorate([
    (0, graphql_1.Field)(() => contacts_graph_1.ContactsGraph),
    __metadata("design:type", contacts_graph_1.ContactsGraph)
], MinuteInstitutionGraph.prototype, "contacts", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Float, { defaultValue: 5 }),
    __metadata("design:type", Number)
], MinuteInstitutionGraph.prototype, "rating", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteInstitutionGraph.prototype, "dateAdded", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], MinuteInstitutionGraph.prototype, "videoURL", void 0);
__decorate([
    (0, graphql_1.Field)(() => minuteInstitution_enum_1.AllowedMinuteInstitutionTypes),
    __metadata("design:type", String)
], MinuteInstitutionGraph.prototype, "type", void 0);
__decorate([
    (0, graphql_1.Field)(() => [minuteWorker_graph_1.MinuteWorkerGraph], { nullable: true }),
    __metadata("design:type", Array)
], MinuteInstitutionGraph.prototype, "workers", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Int, { nullable: true }),
    __metadata("design:type", Number)
], MinuteInstitutionGraph.prototype, "numOfWorkers", void 0);
__decorate([
    (0, graphql_1.Field)(() => [workTime_model_1.WorkTimeGraph], { nullable: true }),
    __metadata("design:type", Array)
], MinuteInstitutionGraph.prototype, "workTimes", void 0);
__decorate([
    (0, graphql_1.Field)(() => [tags_model_1.TagsGraph], { nullable: true }),
    __metadata("design:type", Array)
], MinuteInstitutionGraph.prototype, "tags", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteInstitutionGraph.prototype, "iconPath", void 0);
MinuteInstitutionGraph = __decorate([
    (0, graphql_1.ObjectType)('InstitutionMinute'),
    __metadata("design:paramtypes", [Object])
], MinuteInstitutionGraph);
exports.MinuteInstitutionGraph = MinuteInstitutionGraph;
