import { Contacts } from './contacts.interface';
export declare class ContactsGraph implements Contacts {
    email: string;
    phoneNumber: string;
    whatsApp: string;
    instagram: string;
    constructor(contacts: Partial<Contacts>);
}
