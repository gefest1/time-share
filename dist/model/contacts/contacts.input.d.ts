import { Contacts } from './contacts.interface';
export declare class ContactsInput implements Contacts {
    phoneNumber: string;
    email?: string;
    whatsApp?: string;
    instagram?: string;
}
