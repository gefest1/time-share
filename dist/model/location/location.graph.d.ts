import { Location } from './location.interface';
export declare class LocationGraph implements Location {
    address: string;
    latitude: number;
    longitude: number;
    geo: {
        type: 'Point';
        coordinates: [longitude: number, latitude: number];
    };
    constructor(location: Partial<Location>);
}
