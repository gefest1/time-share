export declare class LocationInput {
    address: string;
    latitude: number;
    longitude: number;
}
