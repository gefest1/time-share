"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Role = exports.City = exports.Sex = void 0;
const graphql_1 = require("@nestjs/graphql");
var Sex;
(function (Sex) {
    Sex["Male"] = "male";
    Sex["Female"] = "female";
    Sex["Undefined"] = "undefined";
})(Sex = exports.Sex || (exports.Sex = {}));
var City;
(function (City) {
    City["Almaty"] = "Almaty";
    City["Astana"] = "Astana";
})(City = exports.City || (exports.City = {}));
var Role;
(function (Role) {
    Role["Client"] = "client";
    Role["Specialist"] = "specialist";
})(Role = exports.Role || (exports.Role = {}));
(0, graphql_1.registerEnumType)(Role, {
    name: 'RoleEnum',
});
(0, graphql_1.registerEnumType)(Sex, {
    name: 'Sex',
});
(0, graphql_1.registerEnumType)(City, {
    name: 'CityEnum',
});
