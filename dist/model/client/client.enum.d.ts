export declare enum Sex {
    Male = "male",
    Female = "female",
    Undefined = "undefined"
}
export declare enum City {
    Almaty = "Almaty",
    Astana = "Astana"
}
export declare enum Role {
    Client = "client",
    Specialist = "specialist"
}
