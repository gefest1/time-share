import { Modify } from '../../utils/modify.type';
import { City, Sex } from './client.enum';
import { Client } from './client.interface';
import { CreditCardGraph } from './utils/creditCard.model';
import { PhotoUrlGraph } from '../photoUrl/photoUrl.graph';
export declare class ClientGraph implements Modify<Client, {
    _id: string;
    creditCards: CreditCardGraph[];
}> {
    _id: string;
    fullName: string;
    phoneNumber: string;
    sex?: Sex;
    email?: string;
    dateOfBirth?: Date;
    photoURL?: PhotoUrlGraph;
    creditCards: CreditCardGraph[];
    debt?: number;
    city: City;
    bonus?: number;
    token?: string;
    isDeleted?: true;
    constructor(client: Partial<Client>);
}
