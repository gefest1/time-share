"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientGraph = void 0;
const graphql_1 = require("@nestjs/graphql");
const client_enum_1 = require("./client.enum");
const creditCard_model_1 = require("./utils/creditCard.model");
const photoUrl_graph_1 = require("../photoUrl/photoUrl.graph");
let ClientGraph = class ClientGraph {
    constructor(client) {
        if (client._id != null)
            this._id = client._id.toHexString();
        if (client.fullName != null)
            this.fullName = client.fullName;
        if (client.sex != null)
            this.sex = client.sex;
        if (client.phoneNumber != null)
            this.phoneNumber = client.phoneNumber;
        if (client.email != null)
            this.email = client.email;
        if (client.photoURL != null)
            this.photoURL = client.photoURL;
        if (client.debt != null)
            this.debt = client.debt;
        if (client.city != null)
            this.city = client.city;
        if (client.creditCards != null)
            this.creditCards = client.creditCards.map((val) => new creditCard_model_1.CreditCardGraph(Object.assign({}, val)));
        if (client.bonus != null)
            this.bonus = client.bonus;
    }
};
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], ClientGraph.prototype, "_id", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], ClientGraph.prototype, "fullName", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], ClientGraph.prototype, "phoneNumber", void 0);
__decorate([
    (0, graphql_1.Field)(() => client_enum_1.Sex, { nullable: true }),
    __metadata("design:type", String)
], ClientGraph.prototype, "sex", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], ClientGraph.prototype, "email", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.GraphQLISODateTime, { nullable: true }),
    __metadata("design:type", Date)
], ClientGraph.prototype, "dateOfBirth", void 0);
__decorate([
    (0, graphql_1.Field)(() => photoUrl_graph_1.PhotoUrlGraph, { nullable: true }),
    __metadata("design:type", photoUrl_graph_1.PhotoUrlGraph)
], ClientGraph.prototype, "photoURL", void 0);
__decorate([
    (0, graphql_1.Field)(() => [creditCard_model_1.CreditCardGraph], { nullable: true }),
    __metadata("design:type", Array)
], ClientGraph.prototype, "creditCards", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Int, { nullable: true }),
    __metadata("design:type", Number)
], ClientGraph.prototype, "debt", void 0);
__decorate([
    (0, graphql_1.Field)(() => client_enum_1.City, { nullable: true }),
    __metadata("design:type", String)
], ClientGraph.prototype, "city", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Float, { defaultValue: 0 }),
    __metadata("design:type", Number)
], ClientGraph.prototype, "bonus", void 0);
__decorate([
    (0, graphql_1.Field)(() => String, { nullable: true }),
    __metadata("design:type", String)
], ClientGraph.prototype, "token", void 0);
__decorate([
    (0, graphql_1.Field)(() => Boolean, { nullable: true }),
    __metadata("design:type", Boolean)
], ClientGraph.prototype, "isDeleted", void 0);
ClientGraph = __decorate([
    (0, graphql_1.ObjectType)('Client'),
    __metadata("design:paramtypes", [Object])
], ClientGraph);
exports.ClientGraph = ClientGraph;
