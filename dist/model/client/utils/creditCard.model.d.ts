import { ObjectId } from 'mongodb';
import { Modify } from '../../../utils/modify.type';
export interface CreditCard {
    _id: ObjectId;
    customerId?: string;
    cardHolderName: string;
    cardId?: string;
    last4Digits: string;
    cardType?: string;
    cardToken?: string;
}
export declare class CreditCardGraph implements Modify<Omit<CreditCard, 'customerId'>, {
    _id: string;
}> {
    _id: string;
    last4Digits: string;
    cardType: string;
    cardId: string;
    cardHolderName: string;
    constructor(creditCard: Partial<CreditCard>);
}
