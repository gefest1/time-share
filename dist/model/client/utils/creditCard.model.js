"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreditCardGraph = void 0;
const graphql_1 = require("@nestjs/graphql");
let CreditCardGraph = class CreditCardGraph {
    constructor(creditCard) {
        if (creditCard._id != null)
            this._id = creditCard._id.toHexString();
        if (creditCard.last4Digits != null)
            this.last4Digits = creditCard.last4Digits;
        if (creditCard.cardType != null)
            this.cardType = creditCard.cardType;
        if (creditCard.cardId != null)
            this.cardId = creditCard.cardId;
        if (creditCard.cardHolderName != null)
            this.cardHolderName = creditCard.cardHolderName;
    }
};
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], CreditCardGraph.prototype, "_id", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], CreditCardGraph.prototype, "last4Digits", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], CreditCardGraph.prototype, "cardType", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], CreditCardGraph.prototype, "cardId", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], CreditCardGraph.prototype, "cardHolderName", void 0);
CreditCardGraph = __decorate([
    (0, graphql_1.ObjectType)(),
    __metadata("design:paramtypes", [Object])
], CreditCardGraph);
exports.CreditCardGraph = CreditCardGraph;
