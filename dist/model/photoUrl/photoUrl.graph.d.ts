import { PhotoUrl } from './photoUrl.interface';
export declare class PhotoUrlGraph implements PhotoUrl {
    XL: string;
    M: string;
    thumbnail: string;
    constructor(photoUrl: Partial<PhotoUrl>);
}
