export interface PhotoUrl {
    thumbnail: string;
    XL: string;
    M: string;
}
