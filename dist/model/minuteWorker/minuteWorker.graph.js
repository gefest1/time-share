"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MinuteWorkerGraph = void 0;
const graphql_1 = require("@nestjs/graphql");
const minuteInstitution_graph_1 = require("../minuteInstitution/minuteInstitution.graph");
let MinuteWorkerGraph = class MinuteWorkerGraph {
    constructor(worker) {
        if (worker._id)
            this._id = worker._id.toHexString();
        if (worker.fullName)
            this.fullName = worker.fullName;
        if (worker.login)
            this.login = worker.login;
        if (worker.institution)
            this.institution = new minuteInstitution_graph_1.MinuteInstitutionGraph(Object.assign({}, worker.institution));
        if (worker.token)
            this.token = worker.token;
    }
};
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteWorkerGraph.prototype, "_id", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteWorkerGraph.prototype, "fullName", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], MinuteWorkerGraph.prototype, "login", void 0);
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], MinuteWorkerGraph.prototype, "token", void 0);
__decorate([
    (0, graphql_1.Field)(() => minuteInstitution_graph_1.MinuteInstitutionGraph, { nullable: true }),
    __metadata("design:type", minuteInstitution_graph_1.MinuteInstitutionGraph)
], MinuteWorkerGraph.prototype, "institution", void 0);
MinuteWorkerGraph = __decorate([
    (0, graphql_1.ObjectType)('Worker'),
    __metadata("design:paramtypes", [Object])
], MinuteWorkerGraph);
exports.MinuteWorkerGraph = MinuteWorkerGraph;
