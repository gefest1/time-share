import { Modify } from '../../utils/modify.type';
import { MinuteInstitutionGraph } from '../minuteInstitution/minuteInstitution.graph';
import { MinuteInstitution } from '../minuteInstitution/minuteInstitution.interface';
import { MinuteWorker } from './minuteWorker.interface';
export declare class MinuteWorkerGraph implements Modify<Omit<MinuteWorker, 'passwordHASH' | 'institutionId'>, {
    _id: string;
}> {
    _id: string;
    fullName: string;
    login: string;
    token: string;
    institution: MinuteInstitutionGraph;
    constructor(worker: Partial<MinuteWorker> & {
        institution?: MinuteInstitution;
        token?: string;
    });
}
