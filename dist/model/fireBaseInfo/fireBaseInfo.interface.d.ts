export interface FireBaseInfo {
    token: string;
    device: string;
}
