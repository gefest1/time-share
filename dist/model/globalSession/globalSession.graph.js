"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimeShareGlobalSessionGraph = exports.TimeShareGlobalSessionDataGraph = void 0;
const graphql_1 = require("@nestjs/graphql");
const globalSession_enum_1 = require("./globalSession.enum");
let TimeShareGlobalSessionDataGraph = class TimeShareGlobalSessionDataGraph {
    constructor(args) {
        if (args.iconPath != null)
            this.iconPath = args.iconPath;
        if (args.institutionId != null)
            this.institutionId = args.institutionId.toHexString();
        if (args.institutionName != null)
            this.institutionName = args.institutionName;
        if (args.price != null)
            this.price = args.price;
        if (args.serviceId != null)
            this.serviceId = args.serviceId.toHexString();
        if (args.serviceName != null)
            this.serviceName = args.serviceName;
    }
};
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], TimeShareGlobalSessionDataGraph.prototype, "institutionId", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], TimeShareGlobalSessionDataGraph.prototype, "institutionName", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Int),
    __metadata("design:type", Number)
], TimeShareGlobalSessionDataGraph.prototype, "price", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], TimeShareGlobalSessionDataGraph.prototype, "serviceId", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], TimeShareGlobalSessionDataGraph.prototype, "serviceName", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], TimeShareGlobalSessionDataGraph.prototype, "iconPath", void 0);
TimeShareGlobalSessionDataGraph = __decorate([
    (0, graphql_1.ObjectType)(),
    __metadata("design:paramtypes", [Object])
], TimeShareGlobalSessionDataGraph);
exports.TimeShareGlobalSessionDataGraph = TimeShareGlobalSessionDataGraph;
let TimeShareGlobalSessionGraph = class TimeShareGlobalSessionGraph {
    constructor(args) {
        if (args._id != null)
            this._id = args._id.toHexString();
        if (args.clientId != null)
            this.clientId = args.clientId.toHexString();
        if (args.isActive != null)
            this.isActive = args.isActive;
        if (args.sessionData != null)
            this.sessionData = new TimeShareGlobalSessionDataGraph(Object.assign({}, args.sessionData));
        if (args.sessionId != null)
            this.sessionId = args.sessionId.toHexString();
        if (args.type != null)
            this.type = args.type;
    }
};
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], TimeShareGlobalSessionGraph.prototype, "_id", void 0);
__decorate([
    (0, graphql_1.Field)(() => TimeShareGlobalSessionDataGraph),
    __metadata("design:type", TimeShareGlobalSessionDataGraph)
], TimeShareGlobalSessionGraph.prototype, "sessionData", void 0);
__decorate([
    (0, graphql_1.Field)(() => globalSession_enum_1.AllowedGlobalSessionTypes),
    __metadata("design:type", String)
], TimeShareGlobalSessionGraph.prototype, "type", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], TimeShareGlobalSessionGraph.prototype, "clientId", void 0);
__decorate([
    (0, graphql_1.Field)(() => Boolean),
    __metadata("design:type", Boolean)
], TimeShareGlobalSessionGraph.prototype, "isActive", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], TimeShareGlobalSessionGraph.prototype, "sessionId", void 0);
TimeShareGlobalSessionGraph = __decorate([
    (0, graphql_1.ObjectType)(),
    __metadata("design:paramtypes", [Object])
], TimeShareGlobalSessionGraph);
exports.TimeShareGlobalSessionGraph = TimeShareGlobalSessionGraph;
