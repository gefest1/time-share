export declare enum AllowedGlobalSessionTypes {
    MINUTEPAY = "minutePay",
    SERVICES = "services",
    HOMEFOOD = "homeFood",
    HOUSES = "houses"
}
