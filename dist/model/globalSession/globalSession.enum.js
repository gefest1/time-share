"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllowedGlobalSessionTypes = void 0;
const graphql_1 = require("@nestjs/graphql");
var AllowedGlobalSessionTypes;
(function (AllowedGlobalSessionTypes) {
    AllowedGlobalSessionTypes["MINUTEPAY"] = "minutePay";
    AllowedGlobalSessionTypes["SERVICES"] = "services";
    AllowedGlobalSessionTypes["HOMEFOOD"] = "homeFood";
    AllowedGlobalSessionTypes["HOUSES"] = "houses";
})(AllowedGlobalSessionTypes = exports.AllowedGlobalSessionTypes || (exports.AllowedGlobalSessionTypes = {}));
(0, graphql_1.registerEnumType)(AllowedGlobalSessionTypes, {
    name: 'AllowedSessionTypes',
});
