import { Modify } from '../../utils/modify.type';
import { AllowedGlobalSessionTypes } from './globalSession.enum';
import { TimeShareGlobalSession, TimeShareGlobalSessionData } from './globalSession.interface';
export declare class TimeShareGlobalSessionDataGraph implements Modify<TimeShareGlobalSessionData, {
    institutionId: string;
    serviceId: string;
}> {
    institutionId: string;
    institutionName: string;
    price: number;
    serviceId: string;
    serviceName?: string;
    iconPath: string;
    constructor(args: Partial<TimeShareGlobalSessionData>);
}
export declare class TimeShareGlobalSessionGraph implements Modify<TimeShareGlobalSession, {
    sessionData: TimeShareGlobalSessionDataGraph;
    clientId: string;
    _id: string;
    sessionId: string;
}> {
    _id: string;
    sessionData: TimeShareGlobalSessionDataGraph;
    type: AllowedGlobalSessionTypes;
    clientId: string;
    isActive: boolean;
    sessionId: string;
    constructor(args: Partial<TimeShareGlobalSession>);
}
