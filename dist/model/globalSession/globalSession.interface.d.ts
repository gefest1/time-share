import { ObjectId } from 'mongodb';
import { AllowedGlobalSessionTypes } from './globalSession.enum';
export interface TimeShareGlobalSessionData {
    institutionName: string;
    institutionId: ObjectId;
    price: number;
    serviceId: ObjectId;
    serviceName?: string;
    iconPath: string;
}
export interface TimeShareGlobalSession {
    _id: ObjectId;
    sessionId: ObjectId;
    type: AllowedGlobalSessionTypes;
    clientId: ObjectId;
    isActive: boolean;
    sessionData: TimeShareGlobalSessionData;
}
