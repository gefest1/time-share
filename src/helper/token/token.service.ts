import * as jwt from 'jsonwebtoken';
import { join } from 'path/posix';
import * as fs from 'fs';
import { signOption, Token } from './token.interface';

export const verifyToken = function (token: string) {
  const publicKeyPath = join(process.cwd(), 'publicKey.cer');
  const publicKey = fs.readFileSync(publicKeyPath, {
    encoding: 'utf-8',
  });
  const tokenObj: Token = jwt.verify(token, publicKey) as Token;
  return tokenObj;
};

export const createToken = function (payload: Token) {
  const privateKeyPath = join(process.cwd(), 'privateKey.cer');
  const privateKey = fs.readFileSync(privateKeyPath, {
    encoding: 'utf-8',
  });
  const token = jwt.sign(payload, privateKey, signOption);
  return token;
};
