import { roleTypes } from '../auth/auth.roles';
import * as jwt from 'jsonwebtoken';

export interface Token {
  _id?: string;
  phoneNumber: string;
  role: roleTypes;
}

export const signOption: jwt.SignOptions = {
  issuer: '',
  subject: '',
  audience: '',
  expiresIn: '365d',
  algorithm: 'RS256',
};
