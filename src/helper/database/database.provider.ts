import { FactoryProvider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Db, MongoClient } from 'mongodb';
import { join } from 'path';

export const DATABASE_CONNECTION = 'DATABASE_CONNECTION';

export const DatabaseProvider: FactoryProvider<Promise<Db>> = {
  provide: 'DATABASE_CONNECTION',
  useFactory: async (configService: ConfigService) => {
    try {
      const client = await MongoClient.connect(
        configService.get('MONGO_URL'),
        // , {
        //   sslCA: join(process.cwd(), 'ca-certificate.crt'),
        //   keepAlive: true,
        //   noDelay: true,
        // }
      );
      const database = client.db(configService.get('MONGO_DB_NAME'));
      return database;
    } catch (e) {
      throw e;
    }
  },
  inject: [ConfigService],
};
