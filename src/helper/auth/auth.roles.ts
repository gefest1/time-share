import { SetMetadata } from '@nestjs/common';

export type roleTypes = 'specialist' | 'client' | 'owner' | 'client' | 'worker';

export const Roles = (...roles: roleTypes[]) => SetMetadata('roles', roles);

export const matchRoles = function (userRole: roleTypes, roles: string[]) {
  const checkRole = roles.find((val) => {
    return val === userRole;
  });
  if (!checkRole) return false;
  return true;
};
