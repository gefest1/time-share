import {
  CanActivate,
  createParamDecorator,
  ExecutionContext,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { TimeShareClientService } from '../../service/client/client.service';
import { MinuteOwnerService } from '../../service/minuteOwner/minuteOwner.service';
import { MinuteWorkerService } from '../../service/minuteWorker';
import { TimeShareSpecialistService } from '../../service/specialist/specialist.service';
import { verifyToken } from '../token/token.service';
import { matchRoles, roleTypes } from './auth.roles';

@Injectable()
export class PreAuthGuard implements CanActivate {
  constructor(
    private clientService: TimeShareClientService,
    private minuteOwnerService: MinuteOwnerService,
    private specialistService: TimeShareSpecialistService,
    private minuteWorkerService: MinuteWorkerService,
    private reflector: Reflector,
  ) {}

  async canActivate(context: ExecutionContext) {
    const request =
      GqlExecutionContext.create(context).getContext().req.headers;
    const { authorization } = request;
    if (!authorization) return false;
    try {
      const payload = verifyToken(authorization);
      if (!payload) return false;
      request.payload = payload;
      const roles = this.reflector.get<roleTypes[]>(
        'roles',
        context.getHandler(),
      );
      const checkRoles = matchRoles(payload.role, roles);
      if (!checkRoles) return false;
      if (payload.role === 'client') {
        const client = payload._id
          ? await this.clientService.findOne({ _id: new ObjectId(payload._id) })
          : await this.clientService.findOne({
              phoneNumber: payload.phoneNumber,
            });
        if (!client) return false;
        request.user = client;
        request.client = client;
      }
      if (payload.role === 'owner') {
        const minuteOwner = await this.minuteOwnerService.findOne({
          _id: new ObjectId(payload._id),
        });
        if (!minuteOwner) return false;
        request.user = minuteOwner;
        request.minuteOwner = minuteOwner;
      }
      if (payload.role === 'specialist') {
        const specialist = await this.specialistService.findOne({
          _id: new ObjectId(payload._id),
        });
        if (!specialist) return false;
        request.user = specialist;
        request.specialist = specialist;
      }
      if (payload.role === 'worker') {
        const minuteWorker = await this.minuteWorkerService.findOne({
          _id: new ObjectId(payload._id),
        });
        request.user = minuteWorker;
        request.minuteWorker = minuteWorker;
      }
      return true;
    } catch (e) {
      throw `${e}`;
    }
  }
}

@Injectable()
export class PreAuthGuardSubscription implements CanActivate {
  constructor(
    private reflector: Reflector,
    private clientService: TimeShareClientService,
    private minuteOwnerService: MinuteOwnerService,
    private specialistService: TimeShareSpecialistService,
    private minuteWorkerService: MinuteWorkerService,
  ) {}

  async canActivate(context: ExecutionContext) {
    const request = GqlExecutionContext.create(context).getContext();
    const { Authorization } = request;
    if (!Authorization) return false;
    try {
      const payload = verifyToken(Authorization);
      if (!payload) return false;
      request.payload = payload;
      const roles = this.reflector.get<roleTypes[]>(
        'roles',
        context.getHandler(),
      );
      const checkRoles = matchRoles(payload.role, roles);
      if (!checkRoles) return false;
      if (payload.role === 'client') {
        const client = payload._id
          ? await this.clientService.findOne({ _id: new ObjectId(payload._id) })
          : await this.clientService.findOne({
              phoneNumber: payload.phoneNumber,
            });
        if (!client) return false;
        request.user = client;
        request.client = client;
      }
      if (payload.role === 'owner') {
        const minuteOwner = await this.minuteOwnerService.findOne({
          _id: new ObjectId(payload._id),
        });
        if (!minuteOwner) return false;
        request.user = minuteOwner;
        request.minuteOwner = minuteOwner;
      }
      if (payload.role === 'specialist') {
        const specialist = await this.specialistService.findOne({
          _id: new ObjectId(payload._id),
        });
        if (!specialist) return false;
        request.user = specialist;
        request.specialist = specialist;
      }
      if (payload.role === 'worker') {
        const minuteWorker = await this.minuteWorkerService.findOne({
          _id: new ObjectId(payload._id),
        });
        request.user = minuteWorker;
        request.minuteWorker = minuteWorker;
      }
      return true;
    } catch (e) {
      throw `${e}`;
    }
  }
}

export const CurrentUser = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request =
      GqlExecutionContext.create(context).getContext().req.headers;
    return request.user;
  },
);

export const CurrentClient = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request =
      GqlExecutionContext.create(context).getContext().req.headers;
    return request.client;
  },
);

export const CurrentMinuteOwner = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request =
      GqlExecutionContext.create(context).getContext().req.headers;
    return request.minuteOwner;
  },
);

export const CurrentMinuteWorker = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request =
      GqlExecutionContext.create(context).getContext().req.headers;
    return request.minuteWorker;
  },
);

export const CurrentSpecialist = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request =
      GqlExecutionContext.create(context).getContext().req.headers;
    return request.specialist;
  },
);

export const CurrentClientSubscription = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request = GqlExecutionContext.create(context).getContext();
    return request.client;
  },
);

export const CurrentSpecialistSubscription = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request = GqlExecutionContext.create(context).getContext();
    return request.specialist;
  },
);

export const CurrentMinuteOwnerSubscription = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request = GqlExecutionContext.create(context).getContext();
    return request.minuteOwner;
  },
);

export const CurrentMinuteWorkerSubscription = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request = GqlExecutionContext.create(context).getContext();
    return request.minuteWorker;
  },
);

export const CurrentUserSubscription = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request = GqlExecutionContext.create(context).getContext();
    return request.user;
  },
);

export const CurrentTokenPayload = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request =
      GqlExecutionContext.create(context).getContext().req.headers;
    return request.payload;
  },
);

export const CurrentTokenPayloadSubscription = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request = GqlExecutionContext.create(context).getContext();
    return request.payload;
  },
);
