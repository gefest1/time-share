import { Module } from '@nestjs/common';
import { TimeShareClientModule } from '../../service/client/client.module';
import { MinuteOwnerModule } from '../../service/minuteOwner/minuteOwner.module';
import { MinuteWorkerModule } from '../../service/minuteWorker/minuteWorker.module';
import { TimeShareSpecialistModule } from '../../service/specialist';

@Module({
  imports: [
    TimeShareClientModule,
    MinuteOwnerModule,
    MinuteWorkerModule,
    TimeShareSpecialistModule,
  ],
  exports: [
    TimeShareClientModule,
    MinuteOwnerModule,
    MinuteWorkerModule,
    TimeShareSpecialistModule,
  ],
})
export class AuthModule {}
