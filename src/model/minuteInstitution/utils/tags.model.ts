import { Field, ObjectType } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { Modify } from '../../../utils/modify.type';
import { AllowedMinuteInstitutionTypes } from '../minuteInstitution.enum';

export interface Tags {
  _id: ObjectId;
  name: string;
  iconSVGPath: string;
  type: AllowedMinuteInstitutionTypes[];
}

@ObjectType('Tags')
export class TagsGraph
  implements
    Modify<
      Tags,
      {
        _id: string;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  iconSVGPath: string;

  @Field()
  name: string;

  @Field(() => [AllowedMinuteInstitutionTypes])
  type: AllowedMinuteInstitutionTypes[];

  constructor(tags: Partial<Tags>) {
    if (tags._id != null) this._id = tags._id.toHexString();
    if (tags.iconSVGPath != null) this.iconSVGPath = tags.iconSVGPath;
    if (tags.name != null) this.name = tags.name;
    if (tags.type != null) this.type = tags.type;
  }
}
