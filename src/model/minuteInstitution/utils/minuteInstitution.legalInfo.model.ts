import { Field, InputType, ObjectType } from '@nestjs/graphql';

export interface MinuteInstitutionLegalInfo {
  UIN?: string;
  requisites?: string;
  legalCompanyName?: string;
}

@ObjectType('MinuteInstitutionLegalInfo')
export class MinuteInstitutionLegalInfoGraph
  implements MinuteInstitutionLegalInfo
{
  @Field({ nullable: true })
  UIN?: string;

  @Field({ nullable: true })
  requisites?: string;

  @Field({ nullable: true })
  legalCompanyName?: string;

  constructor(legalInfo: MinuteInstitutionLegalInfo) {
    if (legalInfo.UIN != null) this.UIN = legalInfo.UIN;
    if (legalInfo.legalCompanyName != null)
      this.legalCompanyName = legalInfo.legalCompanyName;
    if (legalInfo.requisites != null) this.requisites = legalInfo.requisites;
  }
}

@InputType()
export class MinuteInstitutionLegalInfoInput
  implements MinuteInstitutionLegalInfo
{
  @Field({ nullable: true })
  UIN?: string;

  @Field({ nullable: true })
  requisites?: string;

  @Field({ nullable: true })
  legalCompanyName?: string;
}
