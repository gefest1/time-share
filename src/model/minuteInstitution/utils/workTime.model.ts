import { Field, GraphQLISODateTime, ObjectType } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { Modify } from '../../../utils/modify.type';

export interface WorkTime {
  _id: ObjectId;
  startTime: Date;
  endTime: Date;
}

@ObjectType('WorkTime')
export class WorkTimeGraph
  implements
    Modify<
      WorkTime,
      {
        _id: string;
      }
    >
{
  @Field()
  _id: string;

  @Field(() => GraphQLISODateTime)
  startTime: Date;

  @Field(() => GraphQLISODateTime)
  endTime: Date;

  constructor(workTime: Partial<WorkTime>) {
    if (workTime._id != null) this._id = workTime._id.toHexString();
    if (workTime.endTime) this.endTime = workTime.endTime;
    if (workTime.startTime) this.startTime = workTime.startTime;
  }
}
