export * from './minuteInstitution.enum';
export * from './minuteInstitution.graph';
export * from './minuteInstitution.interface';
export * from './utils/tags.model';
export * from './utils/workTime.model';
export * from './utils/minuteInstitution.legalInfo.model';
