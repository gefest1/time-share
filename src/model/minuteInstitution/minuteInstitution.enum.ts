import { registerEnumType } from '@nestjs/graphql';

export enum AllowedMinuteInstitutionTypes {
  ActiveLeisure = 'activeLeisure',
  Fitness = 'fitness',
  Entertainment = 'entertainment',
  BusinessCenter = 'businessCenter',
  SportArena = 'sportArena',
  SpecialEvent = 'specialEvent',
}

registerEnumType(AllowedMinuteInstitutionTypes, {
  name: 'AllowedInstitutionTypes',
});
