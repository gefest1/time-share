import { Field, Float, Int, ObjectType } from '@nestjs/graphql';
import { Modify } from '../../utils/modify.type';
import { ContactsGraph } from '../contacts/contacts.graph';
import { LocationGraph } from '../location/location.graph';
import { MinuteWorkerGraph } from '../minuteWorker/minuteWorker.graph';
import { MinuteWorker } from '../minuteWorker/minuteWorker.interface';
import { PhotoUrlGraph } from '../photoUrl/photoUrl.graph';
import { AllowedMinuteInstitutionTypes } from './minuteInstitution.enum';
import { MinuteInstitution } from './minuteInstitution.interface';
import { MinuteInstitutionLegalInfoGraph } from './utils/minuteInstitution.legalInfo.model';
import { TagsGraph } from './utils/tags.model';
import { WorkTimeGraph } from './utils/workTime.model';

@ObjectType('InstitutionMinute')
export class MinuteInstitutionGraph
  implements
    Modify<
      Omit<MinuteInstitution, 'ownerId' | 'ratingSum' | 'workerIds'>,
      {
        _id: string;
        dateAdded: string;
        galleryURLs: PhotoUrlGraph[];
        avatarURL: PhotoUrlGraph;
        contacts: ContactsGraph;
        location: LocationGraph;
        tags?: TagsGraph[];
        workTimes?: WorkTimeGraph[];
        legalInfo?: MinuteInstitutionLegalInfoGraph;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  name: string;

  @Field(() => LocationGraph)
  location: LocationGraph;

  @Field(() => Int)
  averagePrice: number;

  @Field(() => [PhotoUrlGraph], { nullable: 'itemsAndList' })
  galleryURLs: PhotoUrlGraph[];

  @Field(() => PhotoUrlGraph)
  avatarURL: PhotoUrlGraph;

  @Field({ nullable: true })
  description: string;

  @Field()
  city: string;

  @Field(() => Int, { defaultValue: 0 })
  ratingCount: number;

  @Field(() => ContactsGraph)
  contacts: ContactsGraph;

  @Field(() => Float, { defaultValue: 5 })
  rating: number;

  @Field()
  dateAdded: string;

  @Field({ nullable: true })
  videoURL: string;

  @Field(() => AllowedMinuteInstitutionTypes)
  type: AllowedMinuteInstitutionTypes;

  @Field(() => [MinuteWorkerGraph], { nullable: true })
  workers?: MinuteWorkerGraph[];

  @Field(() => Int, { nullable: true })
  numOfWorkers: number;

  @Field(() => [WorkTimeGraph], { nullable: true })
  workTimes?: WorkTimeGraph[];

  @Field(() => [TagsGraph], { nullable: true })
  tags?: TagsGraph[];

  @Field()
  iconPath: string;

  @Field(() => Boolean, { nullable: true })
  onlyByClientQR?: boolean;

  @Field({ nullable: true })
  legalInfo?: MinuteInstitutionLegalInfoGraph;

  constructor(
    institution: Partial<MinuteInstitution> & {
      workers?: MinuteWorker[];
      numOfWorkers?: number;
    },
  ) {
    if (institution._id != null) this._id = institution._id.toHexString();
    if (institution.avatarURL != null)
      this.avatarURL = new PhotoUrlGraph({ ...institution.avatarURL });
    if (institution.averagePrice != null)
      this.averagePrice = institution.averagePrice;
    if (institution.city != null) this.city = institution.city;
    if (institution.dateAdded != null)
      this.dateAdded = institution.dateAdded.toISOString();
    if (institution.galleryURLs != null)
      this.galleryURLs = institution.galleryURLs.map(
        (val) => new PhotoUrlGraph({ ...val }),
      );
    if (institution.description != null)
      this.description = institution.description;
    if (institution.ratingCount != null)
      this.ratingCount = institution.ratingCount;
    if (institution.ratingSum != null) {
      const rating = institution.ratingSum / institution.ratingCount;
      this.rating = Math.round(rating * 10) / 10;
    }
    if (institution.name != null) this.name = institution.name;
    if (institution.workers != null)
      this.workers = institution.workers.map(
        (val) => new MinuteWorkerGraph({ ...val }),
      );
    if (institution.numOfWorkers != null)
      this.numOfWorkers = institution.numOfWorkers;
    if (institution.location != null)
      this.location = new LocationGraph({ ...institution.location });
    if (institution.contacts != null)
      this.contacts = new ContactsGraph({ ...institution.contacts });
    if (institution.videoURL != null) this.videoURL = institution.videoURL;
    if (institution.workTimes != null)
      this.workTimes = institution.workTimes.map(
        (val) => new WorkTimeGraph({ ...val }),
      );
    if (institution.type != null) this.type = institution.type;
    if (institution.tags != null)
      this.tags = institution.tags.map((val) => new TagsGraph({ ...val }));
    if (institution.iconPath != null) this.iconPath = institution.iconPath;
    if (institution.onlyByClientQR != null)
      this.onlyByClientQR = institution.onlyByClientQR;
    if (institution.legalInfo != null)
      this.legalInfo = new MinuteInstitutionLegalInfoGraph({
        ...institution.legalInfo,
      });
  }
}
