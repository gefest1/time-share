import { ObjectId } from 'mongodb';
import { FireBaseInfo } from '../fireBaseInfo/fireBaseInfo.interface';
import { PhotoUrl } from '../photoUrl/photoUrl.interface';
import { City, Sex } from './client.enum';
import { CreditCard } from './utils/creditCard.model';

export interface Client {
  _id: ObjectId;
  fullName: string;
  phoneNumber: string;
  sex?: Sex;
  email?: string;
  dateOfBirth?: Date;
  photoURL?: PhotoUrl;
  creditCards?: CreditCard[];
  debt?: number;
  city?: City;
  fireBaseToken?: FireBaseInfo[];
  bonus?: number;
  isDeleted?: true;
  iokaInfo?: {
    customer_token: string;
    customer_id: string;
  };
}
