import {
  Field,
  Float,
  GraphQLISODateTime,
  Int,
  ObjectType,
} from '@nestjs/graphql';
import { Modify } from '../../utils/modify.type';
import { City, Sex } from './client.enum';
import { Client } from './client.interface';
import { CreditCardGraph } from './utils/creditCard.model';
import { PhotoUrlGraph } from '../photoUrl/photoUrl.graph';

@ObjectType('Client')
export class ClientGraph
  implements
    Modify<
      Client,
      {
        _id: string;
        creditCards: CreditCardGraph[];
      }
    >
{
  @Field()
  _id: string;

  @Field()
  fullName: string;

  @Field()
  phoneNumber: string;

  @Field(() => Sex, { nullable: true })
  sex?: Sex;

  @Field({ nullable: true })
  email?: string;

  @Field(() => GraphQLISODateTime, { nullable: true })
  dateOfBirth?: Date;

  @Field(() => PhotoUrlGraph, { nullable: true })
  photoURL?: PhotoUrlGraph;

  @Field(() => [CreditCardGraph], { nullable: true })
  creditCards: CreditCardGraph[];

  @Field(() => Int, { nullable: true })
  debt?: number;

  @Field(() => City, { nullable: true })
  city: City;

  @Field(() => Float, { defaultValue: 0 })
  bonus?: number;
  // DELETE TOKEN IN FUTURE PLS
  @Field(() => String, { nullable: true })
  token?: string;

  @Field(() => Boolean, { nullable: true })
  isDeleted?: true;

  constructor(client: Partial<Client>) {
    if (client._id != null) this._id = client._id.toHexString();
    if (client.fullName != null) this.fullName = client.fullName;
    if (client.sex != null) this.sex = client.sex;
    if (client.phoneNumber != null) this.phoneNumber = client.phoneNumber;
    if (client.email != null) this.email = client.email;
    if (client.photoURL != null) this.photoURL = client.photoURL;
    if (client.debt != null) this.debt = client.debt;
    if (client.city != null) this.city = client.city;
    if (client.creditCards != null)
      this.creditCards = client.creditCards.map(
        (val) => new CreditCardGraph({ ...val }),
      );
    if (client.bonus != null) this.bonus = client.bonus;
  }
}
