import { Field, ObjectType } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { Modify } from '../../../utils/modify.type';

export interface CreditCard {
  _id: ObjectId;
  customerId?: string;
  cardHolderName: string;
  cardId?: string;
  last4Digits: string;
  cardType?: string;
  cardToken?: string;
}

@ObjectType()
export class CreditCardGraph
  implements
    Modify<
      Omit<CreditCard, 'customerId'>,
      {
        _id: string;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  last4Digits: string;

  @Field({ nullable: true })
  cardType: string;

  @Field({ nullable: true })
  cardId: string;

  @Field({ nullable: true })
  cardHolderName: string;

  constructor(creditCard: Partial<CreditCard>) {
    if (creditCard._id != null) this._id = creditCard._id.toHexString();
    if (creditCard.last4Digits != null)
      this.last4Digits = creditCard.last4Digits;
    if (creditCard.cardType != null) this.cardType = creditCard.cardType;
    if (creditCard.cardId != null) this.cardId = creditCard.cardId;
    if (creditCard.cardHolderName != null)
      this.cardHolderName = creditCard.cardHolderName;
  }
}
