import { registerEnumType } from '@nestjs/graphql';

export enum Sex {
  Male = 'male',
  Female = 'female',
  Undefined = 'undefined',
}

export enum City {
  Almaty = 'Almaty',
  Astana = 'Astana',
}

export enum Role {
  Client = 'client',
  Specialist = 'specialist',
}

registerEnumType(Role, {
  name: 'RoleEnum',
});

registerEnumType(Sex, {
  name: 'Sex',
});

registerEnumType(City, {
  name: 'CityEnum',
});
