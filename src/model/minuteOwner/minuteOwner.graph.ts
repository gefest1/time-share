import { Field, GraphQLISODateTime, ObjectType } from '@nestjs/graphql';
import { MinuteInstitution } from '../minuteInstitution/minuteInstitution.interface';
import { Modify } from '../../utils/modify.type';
import { MinuteInstitutionGraph } from '../minuteInstitution/minuteInstitution.graph';
import { PhotoUrlGraph } from '../photoUrl/photoUrl.graph';
import { MinuteOwner } from './minuteOwner.interface';

@ObjectType('Owner')
export class MinuteOwnerGraph
  implements
    Modify<
      Omit<MinuteOwner, 'passwordHASH'>,
      {
        _id: string;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  fullName: string;

  @Field()
  email: string;

  @Field()
  phoneNumber: string;

  @Field({ nullable: true })
  token: string;

  @Field(() => Boolean, { nullable: true })
  isNotApproved: boolean;

  @Field({ nullable: true })
  typeOfBusiness: string;

  @Field({ nullable: true })
  nameOfTheOrganization: string;

  @Field(() => PhotoUrlGraph, { nullable: true })
  photoURL: PhotoUrlGraph;

  @Field(() => GraphQLISODateTime)
  dateOfRequest: Date;

  @Field(() => [MinuteInstitutionGraph], { nullable: true })
  institutions: MinuteInstitutionGraph[];

  constructor(
    owner: Partial<MinuteOwner> & {
      token?: string;
      institutions?: MinuteInstitution[];
    },
  ) {
    if (owner._id != null) this._id = owner._id.toHexString();
    if (owner.email != null) this.email = owner.email;
    if (owner.fullName != null) this.fullName = owner.fullName;
    if (owner.phoneNumber != null) this.phoneNumber = owner.phoneNumber;
    if (owner.token != null) this.token = owner.token;
    if (owner.isNotApproved != null) this.isNotApproved = owner.isNotApproved;
    if (owner.typeOfBusiness != null)
      this.typeOfBusiness = owner.typeOfBusiness;
    if (owner.nameOfTheOrganization != null)
      this.nameOfTheOrganization = owner.nameOfTheOrganization;
    if (owner.photoURL != null)
      this.photoURL = new PhotoUrlGraph({ ...owner.photoURL });
    if (owner.institutions && owner.institutions[0] != null)
      this.institutions = owner.institutions.map(
        (val) => new MinuteInstitutionGraph({ ...val }),
      );
  }
}
