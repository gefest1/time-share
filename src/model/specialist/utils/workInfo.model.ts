import { Field, InputType, ObjectType } from '@nestjs/graphql';
import {
  AllowedWorkingNameSpecialistInfo,
  AllowedWorkingTimeSpecialistInfo,
} from './workInfo.enum';

export interface SpecialistWorkingInfo {
  workingName: AllowedWorkingNameSpecialistInfo;
  workingTime: AllowedWorkingTimeSpecialistInfo;
}

@ObjectType()
export class SpecialistWorkingInfoGraph implements SpecialistWorkingInfo {
  @Field(() => AllowedWorkingNameSpecialistInfo)
  workingName: AllowedWorkingNameSpecialistInfo;

  @Field(() => AllowedWorkingTimeSpecialistInfo)
  workingTime: AllowedWorkingTimeSpecialistInfo;

  constructor(args: Partial<SpecialistWorkingInfo>) {
    if (args.workingName != null) this.workingName = args.workingName;
    if (args.workingTime != null) this.workingTime = args.workingTime;
  }
}

@InputType()
export class SpecialistWorkingInfoInput implements SpecialistWorkingInfo {
  @Field(() => AllowedWorkingNameSpecialistInfo)
  workingName: AllowedWorkingNameSpecialistInfo;

  @Field(() => AllowedWorkingTimeSpecialistInfo)
  workingTime: AllowedWorkingTimeSpecialistInfo;
}
