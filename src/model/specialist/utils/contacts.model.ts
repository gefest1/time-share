import { ObjectType } from '@nestjs/graphql';

export interface SpecialistContacts {
  phoneNumber?: string;
  telegram?: string;
  instagram?: string;
  whatsapp?: string;
}
