import { ObjectId } from 'mongodb';
import { PhotoUrl } from '../../../model/photoUrl/photoUrl.interface';
import { registerEnumType } from '@nestjs/graphql';

export enum AllowedDocumentStatuses {
  Rejected = 'rejected',
  Verified = 'verified',
}

registerEnumType(AllowedDocumentStatuses, {
  name: 'AllowedDocumentStatuses',
});

export interface SpecialistDocument {
  _id: ObjectId;
  countryCode: string;
  documentNumber: string;
  photoURLs: PhotoUrl[];
  status?: AllowedDocumentStatuses;
  dateOfRequest: Date;
  reasonOfRejection?: string;
}
