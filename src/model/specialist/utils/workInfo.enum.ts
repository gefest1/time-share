import { registerEnumType } from '@nestjs/graphql';

export enum AllowedWorkingNameSpecialistInfo {
  Company = 'company',
  Private = 'private',
}

export enum AllowedWorkingTimeSpecialistInfo {
  Always = 'always',
  Sometimes = 'sometimes',
  DontKnow = 'dontKnow',
}

registerEnumType(AllowedWorkingNameSpecialistInfo, {
  name: 'AllowedWorkingNameSpecialistInfo',
});

registerEnumType(AllowedWorkingTimeSpecialistInfo, {
  name: 'AllowedWorkingTimeSpecialistInfo',
});
