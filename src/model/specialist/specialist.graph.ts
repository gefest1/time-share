import { Field, GraphQLISODateTime, ObjectType } from '@nestjs/graphql';
import { Modify } from '../../utils/modify.type';
import { Sex } from '../client/client.enum';
import { ContactsGraph } from '../contacts/contacts.graph';
import { PhotoUrlGraph } from '../photoUrl/photoUrl.graph';
import { Specialist } from './specialist.interface';
import {
  SpecialistWorkingInfo,
  SpecialistWorkingInfoGraph,
} from './utils/workInfo.model';

@ObjectType('Specialist')
export class SpecialistGraph
  implements
    Modify<
      Specialist,
      {
        _id: string;
        instituionIds?: string[];
        workInfo?: SpecialistWorkingInfoGraph;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  fullName: string;

  @Field()
  phoneNumber: string;

  @Field(() => Sex)
  sex: Sex;

  @Field()
  description: string;

  @Field({ nullable: true })
  videoUrl: string;

  @Field(() => PhotoUrlGraph, { nullable: true })
  certifications: PhotoUrlGraph[];

  @Field(() => [ContactsGraph], { nullable: true })
  contacts: ContactsGraph[];

  @Field({ nullable: true })
  email: string;

  @Field(() => GraphQLISODateTime, { nullable: true })
  dateOfBirth: Date;

  @Field(() => PhotoUrlGraph, { nullable: true })
  photoURL: PhotoUrlGraph;

  @Field(() => [String], { nullable: true })
  instituionIds?: string[];

  @Field(() => SpecialistWorkingInfoGraph, { nullable: true })
  workInfo?: SpecialistWorkingInfoGraph;

  constructor(specialist: Partial<Specialist>) {
    if (specialist._id != null) this._id = specialist._id.toHexString();
    if (specialist.fullName != null) this.fullName = specialist.fullName;
    if (specialist.phoneNumber != null)
      this.phoneNumber = specialist.phoneNumber;

    if (specialist.sex != null) this.sex = specialist.sex;
    if (specialist.description != null)
      this.description = specialist.description;
    if (specialist.email != null) this.email = specialist.email;
    if (specialist.videoUrl != null) this.videoUrl = specialist.videoUrl;
    if (specialist.photoURL != null)
      this.photoURL = new PhotoUrlGraph({ ...specialist.photoURL });
    if (specialist.contacts != null)
      this.contacts = specialist.contacts.map(
        (val) => new ContactsGraph({ ...val }),
      );
    if (specialist.instituionIds != null)
      this.instituionIds = specialist.instituionIds.map((val) =>
        val.toHexString(),
      );
    if (specialist.certifications != null)
      this.certifications = specialist.certifications.map(
        (val) => new PhotoUrlGraph({ ...val }),
      );
    if (specialist.dateOfBirth != null)
      this.dateOfBirth = specialist.dateOfBirth;
    if (specialist.workInfo != null)
      this.workInfo = new SpecialistWorkingInfoGraph({
        ...specialist.workInfo,
      });
  }
}
