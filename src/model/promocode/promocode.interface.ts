import { ObjectId } from 'mongodb';

export interface TimeSharePromocode {
  _id: ObjectId;
  code: string;
  bonus: number;
  allowedNumOfClients?: number;
  endDate?: Date;
  clientId: ObjectId[];
}
