import { registerEnumType } from '@nestjs/graphql';

export enum AllowedGlobalSessionTypes {
  MINUTEPAY = 'minutePay',
  SERVICES = 'services',
  HOMEFOOD = 'homeFood',
  HOUSES = 'houses',
}

registerEnumType(AllowedGlobalSessionTypes, {
  name: 'AllowedSessionTypes',
});
