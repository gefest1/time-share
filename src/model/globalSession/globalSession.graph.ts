import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Modify } from '../../utils/modify.type';
import { AllowedGlobalSessionTypes } from './globalSession.enum';
import {
  TimeShareGlobalSession,
  TimeShareGlobalSessionData,
} from './globalSession.interface';

@ObjectType()
export class TimeShareGlobalSessionDataGraph
  implements
    Modify<
      TimeShareGlobalSessionData,
      {
        institutionId: string;
        serviceId: string;
      }
    >
{
  @Field()
  institutionId: string;

  @Field()
  institutionName: string;

  @Field(() => Int)
  price: number;

  @Field()
  serviceId: string;

  @Field()
  serviceName?: string;

  @Field()
  iconPath: string;

  constructor(args: Partial<TimeShareGlobalSessionData>) {
    if (args.iconPath != null) this.iconPath = args.iconPath;
    if (args.institutionId != null)
      this.institutionId = args.institutionId.toHexString();
    if (args.institutionName != null)
      this.institutionName = args.institutionName;
    if (args.price != null) this.price = args.price;
    if (args.serviceId != null) this.serviceId = args.serviceId.toHexString();
    if (args.serviceName != null) this.serviceName = args.serviceName;
  }
}

@ObjectType()
export class TimeShareGlobalSessionGraph
  implements
    Modify<
      TimeShareGlobalSession,
      {
        sessionData: TimeShareGlobalSessionDataGraph;
        clientId: string;
        _id: string;
        sessionId: string;
      }
    >
{
  @Field()
  _id: string;

  @Field(() => TimeShareGlobalSessionDataGraph)
  sessionData: TimeShareGlobalSessionDataGraph;

  @Field(() => AllowedGlobalSessionTypes)
  type: AllowedGlobalSessionTypes;

  @Field()
  clientId: string;

  @Field(() => Boolean)
  isActive: boolean;

  @Field()
  sessionId: string;

  constructor(args: Partial<TimeShareGlobalSession>) {
    if (args._id != null) this._id = args._id.toHexString();
    if (args.clientId != null) this.clientId = args.clientId.toHexString();
    if (args.isActive != null) this.isActive = args.isActive;
    if (args.sessionData != null)
      this.sessionData = new TimeShareGlobalSessionDataGraph({
        ...args.sessionData,
      });
    if (args.sessionId != null) this.sessionId = args.sessionId.toHexString();
    if (args.type != null) this.type = args.type;
  }
}
