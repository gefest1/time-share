export interface Contacts {
  phoneNumber: string;
  email?: string;
  whatsApp?: string;
  instagram?: string;
}
