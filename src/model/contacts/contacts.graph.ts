import { Field, ObjectType } from '@nestjs/graphql';
import { Contacts } from './contacts.interface';

@ObjectType('Contacts')
export class ContactsGraph implements Contacts {
  @Field({ nullable: true })
  email: string;

  @Field()
  phoneNumber: string;

  @Field({ nullable: true })
  whatsApp: string;

  @Field({ nullable: true })
  instagram: string;

  constructor(contacts: Partial<Contacts>) {
    if (contacts.email != null) this.email = contacts.email;
    if (contacts.instagram != null) this.instagram = contacts.instagram;
    if (contacts.phoneNumber != null) this.phoneNumber = contacts.phoneNumber;
    if (contacts.whatsApp != null) this.whatsApp = contacts.whatsApp;
  }
}
