import { Field, InputType } from '@nestjs/graphql';
import { Contacts } from './contacts.interface';

@InputType()
export class ContactsInput implements Contacts {
  @Field()
  phoneNumber: string;

  @Field({ nullable: true })
  email?: string;

  @Field({ nullable: true })
  whatsApp?: string;

  @Field({ nullable: true })
  instagram?: string;
}
