import { ObjectId } from 'mongodb';

export interface MinuteWorker {
  _id: ObjectId;
  fullName: string;
  login: string;
  passwordHASH: string;
  institutionId: ObjectId;
}
