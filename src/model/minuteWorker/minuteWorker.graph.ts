import { Field, ObjectType } from '@nestjs/graphql';
import { Modify } from '../../utils/modify.type';
import { MinuteInstitutionGraph } from '../minuteInstitution/minuteInstitution.graph';
import { MinuteInstitution } from '../minuteInstitution/minuteInstitution.interface';
import { MinuteWorker } from './minuteWorker.interface';

@ObjectType('Worker')
export class MinuteWorkerGraph
  implements
    Modify<
      Omit<MinuteWorker, 'passwordHASH' | 'institutionId'>,
      {
        _id: string;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  fullName: string;

  @Field()
  login: string;

  @Field({ nullable: true })
  token: string;

  @Field(() => MinuteInstitutionGraph, { nullable: true })
  institution: MinuteInstitutionGraph;

  constructor(
    worker: Partial<MinuteWorker> & {
      institution?: MinuteInstitution;
      token?: string;
    },
  ) {
    if (worker._id) this._id = worker._id.toHexString();
    if (worker.fullName) this.fullName = worker.fullName;
    if (worker.login) this.login = worker.login;
    if (worker.institution)
      this.institution = new MinuteInstitutionGraph({ ...worker.institution });
    if (worker.token) this.token = worker.token;
  }
}
