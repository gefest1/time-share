export interface Location {
  address: string;
  geo: { type: 'Point'; coordinates: [longitude: number, latitude: number] };
}
