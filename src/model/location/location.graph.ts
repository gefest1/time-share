import { Field, Float, ObjectType } from '@nestjs/graphql';
import { Location } from './location.interface';

@ObjectType('Location')
export class LocationGraph implements Location {
  @Field()
  address: string;

  @Field(() => Float)
  latitude: number;

  @Field(() => Float)
  longitude: number;

  geo: { type: 'Point'; coordinates: [longitude: number, latitude: number] };

  constructor(location: Partial<Location>) {
    if (location.address != null) this.address = location.address;
    if (location.geo.coordinates[0] != null)
      this.longitude = location.geo.coordinates[0];
    if (location.geo.coordinates[1] != null)
      this.latitude = location.geo.coordinates[1];
  }
}
