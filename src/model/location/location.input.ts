import { Field, Float, InputType } from '@nestjs/graphql';

@InputType()
export class LocationInput {
  @Field()
  address: string;

  @Field(() => Float)
  latitude: number;

  @Field(() => Float)
  longitude: number;
}
