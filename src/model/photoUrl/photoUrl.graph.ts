import { Field, ObjectType } from '@nestjs/graphql';
import { PhotoUrl } from './photoUrl.interface';

@ObjectType('PhotoUrl')
export class PhotoUrlGraph implements PhotoUrl {
  @Field()
  XL: string;

  @Field()
  M: string;

  @Field()
  thumbnail: string;

  constructor(photoUrl: Partial<PhotoUrl>) {
    if (photoUrl.M) this.M = photoUrl.M;
    if (photoUrl.XL) this.XL = photoUrl.XL;
    if (photoUrl.thumbnail) this.thumbnail = photoUrl.thumbnail;
  }
}
