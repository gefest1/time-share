import { Inject, Injectable } from '@nestjs/common';
import { Specialist } from '../../model/specialist/specialist.interface';
import { BasicService } from '../../helper/basic.service';
import { DATABASE_CONNECTION } from '../../helper/database/database.provider';
import { Db } from 'mongodb';

@Injectable()
export class TimeShareSpecialistService extends BasicService<Specialist> {
  constructor(@Inject(DATABASE_CONNECTION) private database: Db) {
    super();
    this.dbService = this.database.collection<Specialist>('specialist');
  }
}
