import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../helper/database/database.module';
import { TimeShareSpecialistService } from './specialist.service';

@Module({
  imports: [DatabaseModule],
  providers: [TimeShareSpecialistService],
  exports: [TimeShareSpecialistService],
})
export class TimeShareSpecialistModule {}
