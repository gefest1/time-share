import { Inject, Injectable } from '@nestjs/common';
import { Db } from 'mongodb';
import { BasicService } from '../../helper/basic.service';
import { DATABASE_CONNECTION } from '../../helper/database/database.provider';
import { MinuteOwner } from '../../model/minuteOwner/minuteOwner.interface';

@Injectable()
export class MinuteOwnerService extends BasicService<MinuteOwner> {
  constructor(@Inject(DATABASE_CONNECTION) private database: Db) {
    super();
    this.dbService = this.database.collection<MinuteOwner>('minuteOwner');
  }
}
