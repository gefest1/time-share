import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../helper/database/database.module';
import { MinuteOwnerService } from './minuteOwner.service';

@Module({
  imports: [DatabaseModule],
  providers: [MinuteOwnerService],
  exports: [MinuteOwnerService],
})
export class MinuteOwnerModule {}
