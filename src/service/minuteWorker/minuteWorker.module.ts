import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../helper/database/database.module';
import { MinuteWorkerService } from './minuteWorker.service';

@Module({
  imports: [DatabaseModule],
  providers: [MinuteWorkerService],
  exports: [MinuteWorkerService],
})
export class MinuteWorkerModule {}
