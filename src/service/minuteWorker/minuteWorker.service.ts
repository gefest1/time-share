import { Inject, Injectable } from '@nestjs/common';
import { MinuteWorker } from '../../model/minuteWorker/minuteWorker.interface';
import { BasicService } from '../../helper/basic.service';
import { DATABASE_CONNECTION } from '../../helper/database/database.provider';
import { Db } from 'mongodb';

@Injectable()
export class MinuteWorkerService extends BasicService<MinuteWorker> {
  constructor(@Inject(DATABASE_CONNECTION) private database: Db) {
    super();
    this.dbService = this.database.collection<MinuteWorker>('minuteWorker');
  }
}
