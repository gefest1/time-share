import { Inject, Injectable } from '@nestjs/common';
import { Client } from '../../model/client/client.interface';
import { Db } from 'mongodb';
import { DATABASE_CONNECTION } from '../../helper/database/database.provider';
import { BasicService } from '../../helper/basic.service';

@Injectable()
export class TimeShareClientService extends BasicService<Client> {
  constructor(@Inject(DATABASE_CONNECTION) private database: Db) {
    super();
    this.dbService = this.database.collection<Client>('client');
  }
}
