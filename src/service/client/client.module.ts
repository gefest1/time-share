import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../helper/database/database.module';
import { TimeShareClientService } from './client.service';

@Module({
  imports: [DatabaseModule],
  providers: [TimeShareClientService],
  exports: [TimeShareClientService],
})
export class TimeShareClientModule {}
