import { Inject, Injectable } from '@nestjs/common';
import { TimeShareGlobalSession } from '../../model/globalSession/globalSession.interface';
import { BasicService } from '../../helper/basic.service';
import { DATABASE_CONNECTION } from '../../helper/database/database.provider';
import { Db } from 'mongodb';

@Injectable()
export class TimeShareGlobalSessionService extends BasicService<TimeShareGlobalSession> {
  constructor(@Inject(DATABASE_CONNECTION) private database: Db) {
    super();
    this.dbService = this.database.collection('session');
  }
}
