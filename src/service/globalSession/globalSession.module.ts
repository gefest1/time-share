import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../helper/database/database.module';
import { TimeShareGlobalSessionService } from './globalSession.service';

@Module({
  imports: [DatabaseModule],
  providers: [TimeShareGlobalSessionService],
  exports: [TimeShareGlobalSessionService],
})
export class TimeShareGlobalSession {}
