import { Module } from '@nestjs/common';
import { TimeSharePromocodeService } from './promocode.service';
import { DatabaseModule } from '../../helper/database/database.module';
import { TimeShareClientModule } from '../client/client.module';

@Module({
  imports: [DatabaseModule, TimeShareClientModule],
  providers: [TimeSharePromocodeService],
  exports: [TimeSharePromocodeService],
})
export class TimeSharePromocodeModule {}
