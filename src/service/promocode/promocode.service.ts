import { Inject, Injectable } from '@nestjs/common';
import { BasicService } from '../../helper/basic.service';
import { TimeSharePromocode } from '../../model/promocode/promocode.interface';
import { DATABASE_CONNECTION } from '../../helper/database/database.provider';
import { Db, ObjectId } from 'mongodb';
import { TimeShareClientService } from '../client/client.service';
import { ApolloError } from 'apollo-server-core';

@Injectable()
export class TimeSharePromocodeService extends BasicService<TimeSharePromocode> {
  constructor(
    @Inject(DATABASE_CONNECTION) private database: Db,
    private clientService: TimeShareClientService,
  ) {
    super();
    this.dbService = this.database.collection<TimeSharePromocode>('promocode');
    this.dbService.createIndex({ code: 1 }, { unique: true });
  }

  async use(args: { code: string; clientId: ObjectId }) {
    const { code, clientId } = args;
    const promocode = await this.dbService.findOne({
      code,
      clientId: {
        $not: {
          $elemMatch: { $eq: clientId },
        },
      },
    });
    if (!promocode) throw new ApolloError('promocode not found');
    const currentDate = new Date();
    if (promocode.allowedNumOfClients) {
      if (promocode.allowedNumOfClients == 0)
        throw new ApolloError('limit of people');
      this.updateOne({
        find: { _id: promocode._id },
        update: { allowedNumOfClients: -1 },
        method: '$inc',
      });
    }
    if (promocode.endDate) {
      if (promocode.endDate < currentDate) throw new ApolloError('passed due');
    }
    const client = await this.clientService.updateOne({
      find: { _id: clientId },
      update: { bonus: promocode.bonus },
      method: '$inc',
    });
    if (!client) return false;
    this.updateOneWithOptions({
      findField: ['_id'],
      findValue: [promocode._id],
      updateField: ['clientId'],
      updateValue: [client._id],
      method: '$addToSet',
    });
    return promocode.bonus;
  }
}
